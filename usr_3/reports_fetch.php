<?php

// Array ( [seltype] => branchi [daterange] => 01/01/2020 - 01/22/2020 [fromdate] => 2020-01-01 [todate] => 2020-01-22 [branch] => ABUROAD [memoid] => )
 
	require('connect.php');
 	error_reporting(0);
    $seltype = $conn_rrpl -> real_escape_string($_POST['seltype']);
    $daterange = $conn_rrpl -> real_escape_string($_POST['daterange']);
    $branch = $conn_rrpl -> real_escape_string($_POST['branch']);
    $lrno = $conn_rrpl -> real_escape_string($_POST['lrno']); 

function before ($thiss, $inthat)
{
    return substr($inthat, 0, strpos($inthat, $thiss));
}

function after ($thiss, $inthat)
{
    if (!is_bool(strpos($inthat, $thiss)))
        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
}
 
$fromdate = before('-', $daterange); 
$fromdate = strtotime($fromdate); 
$todate = after('-', $daterange); 
$todate = strtotime($todate);


    if($seltype=="brancho"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Vou No </th> 
				<th style=""> LR No </th> 
		        <th style=""> FM Date </th>
		        <th style=""> Amount </th> 
		        <th style=""> POD Date </th> 
		        <th style=""> Extended </th> 
		        <th style=""> Upload </th> 
		        <th style=""> Pending </th> 
		        <th style=""> Consignor </th> 
		        <th style=""> Billing Party </th> 
		        <th style=""> Branch </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_lr_outward.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    } else if($seltype=="branchi"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Vou No </th> 
				<th style=""> LR No </th> 
		        <th style=""> FM Date </th>
		        <th style=""> Amount </th> 
		        <th style=""> Upload </th> 
		        <th style=""> POD Date </th> 
				<th style=""> Branch </th>  
		        <th style=""> Billing Date </th> 
		        <th style=""> Billing Party </th> 
		        <th style=""> Intermemo </th> 
		        <th style=""> Received </th> 
		        <th style=""> Narration </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_lr_inward.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
    }  else if($seltype=="memono"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th > Memo No </th>
				<th > Billing Party </th>
				<th > Pod Branch </th>
				<th > Bill Branch </th>
				<th > Dispatch Date </th>
				<th > Sended By </th>
				<th > Narration </th>
				<th > LR\'s </th>
				<th > Collect Date </th>
				<th > Pending </th>
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_memo.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" }, 
		{ mData: "9" }, 
		{ mData: "10" } 
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

    }  else if($seltype=="lrno"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Vou No </th> 
				<th style=""> LR No </th> 
		        <th style=""> FM Date </th>
		        <th style=""> FM Amount </th> 
		        <th style=""> Upload </th> 
		        <th style=""> POD Date </th> 
				<th style=""> POD Branch </th>  
		        <th style=""> Billing Date </th> 
		        <th style=""> Billing Party </th> 
		        <th style=""> Billing Branch </th> 
		        <th style=""> Intermemo </th> 
		        <th style=""> Received Date</th> 
		        <th style=""> Narration </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "reports_lrwise.php?p='.$lrno.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
    }  