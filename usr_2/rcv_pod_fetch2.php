<?php

  require('connect.php');
  
  $id = $conn_rrpl->real_escape_string($_REQUEST['id']);

	$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
	$statement = $connection->prepare("SELECT r.id, m.bilty_no, m.plr, m.date as mdate, m.tamt, r.date, r.copy, r.dispatch, r.dispatch_time, r.collect, r.collect_time, r.branch FROM `mkt_bilty` m left join dairy.rcv_pod r on r.lrno = m.bilty_no where r.memono='$id' and m.billing_branch='$branchuser' and (r.collect='0' or r.collect='-1') and r.dispatch='1'");
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();

$sno = 0;
foreach($result as $row)
{ 
  $sno=$sno+1;
	$sub_array = array(); 
 
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["bilty_no"]; 
  $sub_array[] = $row["plr"]; 
  $sub_array[] = $row["mdate"]; 
  $sub_array[] = $row["tamt"]; 
$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['copy']) as $pod_copies)
{
  $copy_no++;
  $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>Upload: $copy_no</a>";
 }
  $sub_array[] = implode(", ",$pod_files1);
   $sub_array[] = $row["date"]; 
 
  $btn= "<center> <button onclick='action2(".$row['id'].", this.id, \"$id\")' id='select' class='btn btn-sm btn-success' > <i class='fa fa-check '></i> RECIEVED  </button> </center> ";
 
	$sub_array[] = $btn; 

  $sub_array[] = "<center> <button onclick='action2(".$row['id'].", this.id, \"$id\")' id='reject' class='btn btn-sm btn-danger'> <i class='fa fa-trash '></i> CANCEL  </button> </center>  ";   

	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 