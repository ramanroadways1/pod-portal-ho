<?php
 
  require('connect.php');

  // print_r($_POST);

  $sendby = $conn_rrpl->real_escape_string($_POST['sendby']);
  $date = $conn_rrpl->real_escape_string($_POST['date']); 
  $billbranch = $conn_rrpl->real_escape_string($_POST['billbranch']);
  $billparty = $conn_rrpl->real_escape_string($_POST['billparty']);
  $billpartyid = $conn_rrpl->real_escape_string($_POST['billpartyid']);
 
if(isset($_POST['val1']))
{
  $val1 = $conn_rrpl->real_escape_string($_POST['val1']);
} else {
  $val1 = "";
}

if(isset($_POST['val2']))
{
  $val2 = $conn_rrpl->real_escape_string($_POST['val2']);
} else {
  $val2 = "";
}

if(isset($_POST['val3']))
{
  $val3 = $conn_rrpl->real_escape_string($_POST['val3']);
} else {
  $val3 = "";
}

if(isset($_POST['val4']))
{
  $val4 = $conn_rrpl->real_escape_string($_POST['val4']);
} else {
  $val4 = "";
}

if(isset($_POST['val5']))
{
  $val5 = $conn_rrpl->real_escape_string($_POST['val5']);
} else {
  $val5 = "";
}

if(isset($_POST['val6']))
{
  $val6 = $conn_rrpl->real_escape_string($_POST['val6']);
} else {
  $val6 = "";
}

if(isset($_POST['val7']))
{
  $val7 = $conn_rrpl->real_escape_string($_POST['val7']);
} else {
  $val7 = "";
}

if(isset($_POST['val8']))
{
  $val8 = $conn_rrpl->real_escape_string($_POST['val8']);
} else {
  $val8 = "";
}
  
if(!empty($_POST['mark']) || !empty($_POST['mark2']))
{ 

 	$todaydate = date('Y-m-d');
 	$sqli = $conn_rrpl->query("select count(id) from podmemo where date(dispatchdate)='$todaydate'");
 	$resi = $sqli->fetch_assoc();
 	$getlast = $resi['count(id)']+1;
 
    $narration = "NA";
  if($sendby=="COURIER"){
    $narration = "Courier Name: ".$val1." / Docket No: ".$val2;
  } else if($sendby=="TRUCK"){
    $narration = "Truck No: ".$val5." / Driver Name: ".$val6." / Driver Mobile: ".$val7;
  } else if($sendby=="PERSON"){
    $narration = "Person Name: ".$val3." / Person Mobile: ".$val4;
  } else if($sendby=="OTHERS"){
    $narration = "Others: ".$val8;
  }

  	$totallr = "0";
  	$totalbty = "0";
 	$memo = substr($branchuser, 0, 3).substr($billbranch, 0, 3).date('dmY').$getlast;

  	if(!empty($_POST['mark'])){
		$totallr = count($_POST['mark']); 
		$all_lrs = array(); 
		foreach ($_POST['mark'] as $id) { 
			$all_lrs[] = $id; 
			$sql = $conn_rrpl->query("UPDATE rcv_pod SET billing='1', billing_ofc='$billbranch', memono='$memo', billing_time='$sysdatetime', narration='$narration' WHERE id='$id'");
		} 
		$lrs = implode(",",$all_lrs);
	} else {
		$lrs = "";
	} 
 
  	if(!empty($_POST['mark2'])){
		$totalbty = count($_POST['mark2']); 
		$all_bty = array();
		foreach ($_POST['mark2'] as $id) { 
			$all_bty[] = $id; 
			$sql = $conn_dairy->query("UPDATE rcv_pod SET dispatch='1', memono='$memo', dispatch_time='$sysdatetime' WHERE id='$id'");
		}
		$bty = implode(",",$all_bty);
	} else {
		$bty = "";
	}

	$total = $totallr + $totalbty;

 	$sql = $conn_rrpl->query("INSERT INTO podmemo (bilty, memono, bill_party, party_id, branch, bill_branch, dispatchdate, sentby, couriername, docketno, narration, contactname, contactmobile, truckno, drivername, drivermobile, lrsno, remainLR) VALUES ('$bty' ,'$memo', '$date', '$billpartyid', '$branchuser', '$billbranch', '$sysdatetime', '$sendby', '$val1', '$val2', '$val8', '$val3', '$val4', '$val5', '$val6','$val7','$lrs', '$total')");
 	if($sql == TRUE){
		echo "
		<script>
		Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'Intermemo Updated.',
		showConfirmButton: false,
		timer: 1500
		})
		</script>"; 
 	} else {
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: '".mysqli_error($conn_rrpl)."'
		})
		</script>";  
 	}
} else {

	echo "
	<script>
	Swal.fire({
	icon: 'error',
	title: 'Error !!!',
	text: 'No LRs were selected !'
	})
	</script>";  

}