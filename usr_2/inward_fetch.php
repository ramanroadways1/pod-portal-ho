<?php

  require('connect.php');
  
	$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
	$statement = $connection->prepare("select * from podmemo where bill_branch='$branchuser' and remainLR!='0' order by id desc");
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();

foreach($result as $row)
{ 
	$sub_array = array(); 
  
  $btn= "<center><a href='rcv_pod_index.php?id=".$row['memono']."' class='btn btn-sm btn-primary' style='margin-left: 20px; color: #fff; margin-right: 20px; letter-spacing: 1px;'> <i class='fa fa-list'></i> VIEW  </a> </center>"; 
  $sub_array[] = $btn; 
	$sub_array[] = $row["memono"];
  // $sub_array[] = $row["bill_party"]; 
  $sub_array[] = $row["branch"]; 
  $sub_array[] = $row["bill_branch"]; 
  $sub_array[] = $row["dispatchdate"]; 
  $sub_array[] = $row["sentby"]; 

    $narration = "NA";
  if($row["sentby"]=="COURIER"){
    $narration = "Courier Name: ".$row['couriername']." / Docket No: ".$row['docketno'];
  } else if($row["sentby"]=="TRUCK"){
    $narration = "Truck No: ".$row['truckno']." / Driver Name: ".$row['drivername']." / Driver Mobile: ".$row['drivermobile'];
  } else if($row["sentby"]=="PERSON"){
    $narration = "Person Name: ".$row['contactname']." / Person Mobile: ".$row['contactmobile'];
  } else if($row["sentby"]=="OTHERS"){
    $narration = "Others: ".$row['narration'];
  }


  $sub_array[] = $narration; 
  $sub_array[] = $row["remainLR"]; 
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 