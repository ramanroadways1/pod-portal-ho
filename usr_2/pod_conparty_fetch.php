<?php require('connect.php'); ?>

<style type="text/css">
	.applyBtn{
		border-radius: 0px !important;
	}
	.show-calendar{
		top: 180px !important;
	} 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
    .dt-buttons{float: right;}
    .user_data_filter{
        float: right;
    }


    .dt-button {
        padding: 4px 20px;
        text-transform: uppercase;
        font-size: 11px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 3px #999;
    }

    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:1px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444; text-transform: uppercase; } .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
</style>


	<div class="row">
	<div class="col-md-7">
			<div class="card shadow"> 
<div class="card-header">
	<div class="card-body table-responsive" style="overflow:auto">
	<table class="table table-bordered table-hover" style="background-color: #fff;">
	<thead class="thead-light">
	<th style="text-align: center;">Id</th>
	<th style="text-align: center;">Billing Branch</th>
	<!-- <th style="text-align: center;">Billing Party</th> -->
	<th style="text-align: center;">No of LR's</th>
	<th style="text-align: center;">Update</th>
	</thead>							
<?php
	$sql = $conn_rrpl->query("SELECT COUNT(id) as total, billing_party, billing_ofc FROM rcv_pod where nullify='0' and self='0' and  branch='$branchuser' AND  billing!='1' AND billing_party!='0' and billing_ofc!='' GROUP BY billing_ofc");
	if($sql->num_rows>0){

		$i=1;
		while($res = $sql->fetch_assoc()){
			$sqi = $conn_rrpl->query("select * from billing_party where id='$res[billing_party]'");
			$row = $sqi->fetch_assoc();

			$xsql = $conn_rrpl->query("SELECT count(r.id) as cnt FROM `mkt_bilty` m left join dairy.rcv_pod r on r.lrno = m.bilty_no where m.billing_branch='$res[billing_ofc]' and r.copy is not null and r.dispatch='0'");
			$xrow = $xsql->fetch_assoc();
			 $tot = $res['total'] + $xrow['cnt'];

			echo "<tr> <td style='font-size:14px !important; color: #444 !important;'> <center> ".$i." </center> </td>";
			echo " <td style='font-size:14px !important; color: #444 !important;'> <center> ".$res['billing_ofc']." </center></td>";
			// echo " <td> ".$row['name']." </td>";
			echo " <td style='font-size:14px !important; color: #444 !important;'> <center> ".$tot." </center></td>";
			echo ' <td style="text-align: center; "> <a style="font-size:11px"  href="pod_view.php?id='.$res["billing_ofc"].'" class="btn btn-sm btn-warning"> <i class="fa fa-file-text-o" aria-hidden="true"></i> View  </a> </td> </tr>';
			$i=$i+1;
		}

	} else {
			echo "<tr> <td colspan='5'> No Records Found ! </td> </tr>";
	}
?>
		</table>
		</div>
		</div>
		</div>
		</div>
		<!-- <div class="col-md-5">
			<div class="card shadow"> 
<div class="card-header">
			<div class=" card-body table-responsive" style="overflow:auto; padding-top: 0px;">
			<table class="table table-bordered table-hover" style="background-color: #fff;">
			<thead class="thead-light">
			<th style="text-align: left; background: #fff;" colspan="4">Billing Branch Not Given !</th> 
			</thead>
			<thead class="thead-light">
			<th style="text-align: center;">LR</th>
			<th style="text-align: center;">Consignor</th>
			<th style="text-align: center;">Billing Branch</th>
			<th style="text-align: center;">Update</th>
			</thead>							
			<?php
	// 		$sql = $conn_rrpl->query("SELECT COUNT(id) as total, consignor_id FROM rcv_pod where  nullify='0' and self='0' and  branch='$branchuser' AND billing_party='0' GROUP BY consignor_id"); 
	// 		if($sql->num_rows>0){

	// 		$i=1;
	// 		while($res = $sql->fetch_assoc()){
			
	// 		$sqi = $conn_rrpl->query("select * from consignor where id='$res[consignor_id]'");
	// 		$row = $sqi->fetch_assoc();
	// 		$cname = $row['name'];
	// 		$conid = $row["id"];

	// 		if($cname==""){
	// 			$cname = "Consignor Not Available (NA)";
	// 			// $selectparty = "NA";
	// 		}

	// 		$billparty = $row['bill_party_id'];
	// 		$addonparty = $row['addon_party'];

	// 		$allparty = array();
	// 		$allparty[0] = $billparty;
	// 		$allparty[1] = $addonparty;
 

	// 		$allnew = implode(',', $allparty);
	// 		$billparty = explode(',', $allnew);

 // 	$getparty = array();
	// foreach($billparty as $key){
	// 	$sqli = $conn_rrpl->query("select * from billing_party where id='$key'");
	// 	$resi = $sqli->fetch_assoc(); 
	// 	if($key!=='0' && $key!==''){
	// 		$getparty[] = $key." : ".$resi['bill_branch']; 
	// 	} 
	// } 
	// $getparty = implode(',', $getparty);

	// $arrInfo = explode(",",$getparty);

	// $newArray = [];
	// foreach($arrInfo as $item) {
	// $values = explode(":",$item);
	// $newArray[$values[0]] = $values[1];
	// }
 //    $newArray1 = array_unique($newArray,SORT_REGULAR); 
	// // $newArray1 = print_r($newArray1);
 	
	// 			$selectparty = "";
	// 			$selectparty.= "<select name='party' id='party".$conid."' style='max-width:150px; min-width:150px;' required='required'>";
	// 			$selectparty.= "<option value=''> -- select -- </option>";
	// 			foreach ($newArray1 as $key => $value) {
					
	// 				$selectparty.= "<option value='".$key."'> ".$value." </option>";

	// 			} 
	// 			$selectparty.= "</select>";
	  			
	// 			echo "<tr><td><center>".$res['total']."</center></td>";
	// 			echo "<td>".$cname." <input type='hidden' id='con".$conid."' name='conid' value='".$conid."'></td>";
	// 			echo "<td>".$selectparty."</td>";
	// 			echo '<td style="text-align: center; color: #fff;"> <button style="letter-spacing: 0.5px !important;" type="button" onclick="update('.$conid.')" class="btn btn-sm btn-warning"> <i class="fa fa-check-square-o" aria-hidden="true"></i> SAVE </button> </td> </tr>'; 
	// 			}

	// 		} else {
	// 		echo "<tr> <td colspan='5'> No Records Found ! </td> </tr>";
	// 		}
			?>
			</table>
			</div>	
			</div>	
			</div>	
		</div> -->
	</div>
