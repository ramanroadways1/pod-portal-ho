<?php

require('connect.php'); 

  $id = $conn_rrpl->real_escape_string($_REQUEST['id']); 
  $action = $conn_rrpl->real_escape_string($_REQUEST['act']); 
  $memo = $conn_rrpl->real_escape_string($_REQUEST['memo']); 

if($action=="select"){

	$qry = mysqli_query($conn_dairy,"UPDATE rcv_pod SET collect='1', collect_time='$sysdatetime' WHERE id='$id' and dispatch='1' and (collect='0' or collect='-1' )");
	if(!$qry)
	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: '".mysqli_error($conn_dairy)."'
		})
		</script>";  
		exit();
	} else {
		$qry = mysqli_query($conn_rrpl, "UPDATE podmemo set remainlr=remainlr-1, collectdate='$sysdatetime'  where memono='$memo'");
		if(!$qry)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '".mysqli_error($conn_rrpl)."'
			})
			</script>";  
			exit();
		}else{ 
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'LR Accepted.',
			showConfirmButton: false,
			timer: 1000
			})
			</script>"; 
		}
	}
	//$qry = mysqli_query($conn_rrpl,"insert into rcv_pod_log (rcv_pod_id, action, timestamp) values ('$id','ACCEPT','$sysdatetime')");  

} else if ($action=="reject"){

	$qry = mysqli_query($conn_dairy,"UPDATE rcv_pod SET dispatch='0', collect='-1', collect_time=NULL WHERE id='$id' and dispatch='1' and (collect='0' or collect='-1' )");
	if(!$qry)
	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: '".mysqli_error($conn_rrpl)."'
		})
		</script>";  
		exit();
	}  else {
		$qry = mysqli_query($conn_rrpl, "UPDATE podmemo set remainlr=remainlr-1, collectdate='$sysdatetime' where memono='$memo'");
		if(!$qry)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '".mysqli_error($conn_rrpl)."'
			})
			</script>";  
			exit();
		} else { 
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'LR Cancelled.',
			showConfirmButton: false,
			timer: 1000
			})
			</script>"; 
		}
	}
	//$qry = mysqli_query($conn_rrpl,"insert into rcv_pod_log (rcv_pod_id, action, timestamp) values ('$id','CANCEL','$sysdatetime')");  
}
 
?>

<?php

mysqli_close($conn_rrpl);  

?>