<?php

  require('connect.php');
 
   // $p = $conn_rrpl->real_escape_string($_REQUEST['p']);
   $f = $conn_rrpl->real_escape_string($_REQUEST['f']);
   $t = $conn_rrpl->real_escape_string($_REQUEST['t']);

$f = date("Y-m-d",$f);
$t = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
 
$statement = $connection->prepare("SELECT * FROM credit WHERE section='FREIGHT' AND date BETWEEN '$f' AND '$t' ORDER BY id ASC");
  
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array();  

        $sub_array[] = $row["credit_by"];
        $sub_array[] = $row["tno"];
        $sub_array[] = $row["date"];
        $sub_array[] = $row["lr_date"]; 
        $sub_array[] = $row["from_stn"]; 
        $sub_array[] = $row["to_stn"]; 
        $sub_array[] = $row["company"]; 
        $sub_array[] = $row["bilty_no"]; 
        $sub_array[] = $row["amount"]; 
        $sub_array[] = $row["chq_no"]; 
        $sub_array[] = $row["branch"]; 
        $sub_array[] = $row["narr"]; 
              
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 