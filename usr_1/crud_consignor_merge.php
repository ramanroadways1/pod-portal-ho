<?php
	require('connect.php'); 
	$id =  $conn_rrpl -> real_escape_string($_POST['id']);
  
    $members = $conn_rrpl->query("SELECT * FROM `consignor` where id=$id");
    $mem = mysqli_fetch_assoc($members); 
?>
<script type="text/javascript">
	$(function() {
		$("#aftername").autocomplete({
		source: 'crud_consignor_auto.php',
		appendTo: '#appenddiv',
		select: function (event, ui) { 
               $('#aftername').val(ui.item.value);   
               $('#afterid').val(ui.item.dbid);      
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Consignor does not exists !'
			})
			$("#aftername").val("");
			$("#aftername").focus();
		}
		}, 
		focus: function (event, ui){
		return false;
		}
		});
	});
</script>
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}

#appenddiv {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
</style>
<form method="post" action="" id="mergereq" role="form" autocomplete="off">
		<div class="modal-body">
			<p style="color: #444;"> MERGE CONSIGNOR  <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
			</p> 
			<div class="row">
		 		<div class="form-group col-md-12">
						<label for="phone">CONSIGNOR (BEFORE) </label>
						<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" name="beforename" value="<?php echo $mem['name'];?>" readonly />
						<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="hidden" name="beforeid" value="<?php echo $mem['id'];?>" readonly />
				</div>  
				<div class="form-group col-md-12" id="">
						<label for="phone">CONSIGNOR (AFTER) </label>
						<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="aftername" name="aftername" value=""/>
						<div id="appenddiv"></div>
						<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="hidden" id="afterid" value="" name="afterid">
				</div> 
		   </div> 
		</div>
		<div class="modal-footer">
			<button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
			<input type="submit" id="mergereqbtn" class="btn btn-primary" name="submit" value="UPDATE" />
		</div>
</form> 
 
<?php
mysqli_close($conn_rrpl);
?> 