<?php
	require('connect.php');  
?>

<script type="text/javascript">
function gstf(gst){
    var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
    
    if (gstinformat.test(gst)) {
     return true;
    } else {
 		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Please Enter Valid GSTIN Number !'
		}) 
		$("#gst").val('');
        $("#gst").focus();
    }
    
}
</script>
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}
</style>
<form method="post" action="" id="addreq" role="form" autocomplete="off">
	<div class="modal-body">
		<p style="color: #444;"> ADD NEW CONSIGNEE  <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
		</p>
 
		<div class="row"> 
		<div class="form-group col-md-6">
				<label for="phone">  NAME <font color="red"><sup>*</sup></font></label>
				<input type="text"  oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')" class="form-control" id="" name="name" required/>
		</div>
		<div class="form-group col-md-6">
				<label for="phone"> GST  </label>
				<input type="text" onchange="gstf(this.value)"  oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')"  class="form-control" id="gst" name="gst" />
		</div>
		<div class="form-group col-md-6">
				<label for="phone"> MOBILE  </label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')"  class="form-control" id="" name="mobile" />
		</div>
		
		<div class="form-group col-md-6">
				<label for="phone"> PIN CODE <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')"  class="form-control" required id="" name="pincode" />
		</div> 
		
		<div class="form-group col-md-12">
				<label for="phone"> ADDRESS <font color="red"><sup>*</sup></font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')"  required class="form-control" id="" name="address"></textarea>
		</div>
	   </div> 
	</div>
	<div class="modal-footer">
		<button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
		<input type="submit" id="updatereqbtn" class="btn btn-primary" name="submit" value="SAVE" />
	</div>
	</form> 
 
<?php
mysqli_close($conn_rrpl);
?> 