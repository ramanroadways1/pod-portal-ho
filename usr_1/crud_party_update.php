<?php
	require('connect.php'); 
	$id =  $conn_rrpl -> real_escape_string($_POST['id']);
  
    $members = $conn_rrpl->query("SELECT * FROM `billing_party` where id=$id");
    $row = mysqli_fetch_assoc($members); 
?>

<script type="text/javascript">
function gst(gst){
    var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
    
    if (gstinformat.test(gst)) {
     return true;
    } else {
 		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Please Enter Valid GSTIN Number !'
		}) 
		$("#gst_no").val('');
        $("#gst_no").focus();
    }
    
}
</script>	
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}
</style>
<form method="post" action="" id="updatereq" role="form" autocomplete="off">
	<div class="modal-body">
<p style="color: #444;"> UPDATE BILLING PARTY  <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
		</p>

		
		<div class="row">
 		  			<div class="form-group col-md-4">
                      <label for="consignor_name">Party Name </label>
					  <input type="text" name="name" class="form-control"  value="<?php echo $row['name']; ?>"  oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')" >
					  <input type="hidden" name="id" value="<?php echo $row['id']; ?>"  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&]/,'')">
                    </div>
  					
					<div class="form-group col-md-4">
						<label for="consignor_name">Billing Branch </label>
						<select class="form-control" name="branch" required="">  
							<option value=""> -- select -- </option>
						<?php
						 $sql2 = $conn_rrpl -> query("select username as branch from user where role='2' order by username asc");

						 while ($row2 = $sql2->fetch_assoc()) {
						 	$prtslt = "";
						 	if($row['bill_branch']==$row2['branch']){
						 		$prtslt = "selected";
						 	}
						 	echo "<option value='".$row2['branch']."' ".$prtslt."> ".$row2['branch']." </option>";
						 }

						?>
						</select>
                     </div>
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">GST Number</label>
					  <input type="text" name="gst_no" id="gst_no" value="<?php echo $row['gst_no']; ?>" onchange="gst(this.value)" class="form-control">
					</div>
					

					<div class="form-group col-md-12">
                      <label for="consignor_name">Billing Address </label>
					  <input name="addr" value="<?php echo $row['billing_addr']; ?>" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')"  class="form-control">
                    </div>

					<div class="form-group col-md-4">
                      <label for="consignor_name">Email Id</label>
					  <input type="email" name="email" value="<?php echo $row['email_id']; ?>" class="form-control"  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&@]/,'')">
					</div>
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">Mobile Number</label>
					  <input type="text" name="mobile_no" value="<?php echo $row['mobile']; ?>" class="form-control" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')" >
					</div>
					
					 <div class="form-group col-md-4">
                      <label for="consignor_name">Payment Terms </label>
					  <input type="text" class="form-control" value="<?php echo $row['payment_term']; ?>" name="payment_terms"  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&]/,'')">
                    </div>
					
					 <div class="form-group col-md-4">
                      <label for="consignor_name">Rate Frequency  (In days) </label>
					  <input type="number"  value="<?php echo $row['rate_fq']; ?>" class="form-control" name="rate_frq" placeholder="" oninput="this.value=this.value.replace(/[^0-9]/,'')">
                    </div>
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">Next Rate Date</label>
					  <input type="date" value="<?php echo $row['rate_change_date']; ?>" name="rate_change_date" min="<?php echo date('Y-m-d'); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control">
                    </div>
					 
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">EMD</label>
					  <input type="text" name="emd" value="<?php echo $row['emd']; ?>" class="form-control"  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&]/,'')">
					</div>

					
					<div class="form-group col-md-3">
                      <label for="consignor_name">In-Charge Name </label>
					  <input type="text" name="incharge" value="<?php echo $row['incharge_name']; ?>" class="form-control" required  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&]/,'')">
                    </div>
					
					<div class="form-group col-md-3">
                      <label for="consignor_name">In-Charge Mobile </label>
					  <input type="text" name="incharge_mobile" value="<?php echo $row['incharge_mobile']; ?>" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required>
                    </div>
					
					<div class="form-group col-md-3">
                      <label for="consignor_name">Contract Starts From </label>
					  <input type="date" name="con_start" value="<?php echo $row['contract_from']; ?>" max="<?php echo date('Y-m-d'); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control">
                    </div>
					
					<div class="form-group col-md-3">
                      <label for="consignor_name">Contract End Date </label>
					  <input type="date" name="con_end" value="<?php echo $row['contract_to']; ?>" min="<?php echo date('Y-m-d'); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control">
                    </div>
	   </div> 
	</div>
		<div class="modal-footer">
			<button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
			<input type="submit" id="updatereqbtn" class="btn btn-primary" name="submit" value="UPDATE" />
		</div>
	</form> 
 
<?php
mysqli_close($conn_rrpl);
?> 