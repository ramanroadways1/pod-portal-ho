<?php 
 
	require('connect.php');
  
    $id = $conn_rrpl -> real_escape_string($_POST['id']); 
    $code = $conn_rrpl -> real_escape_string($_POST['code']); 
    $mobile = $conn_rrpl -> real_escape_string($_POST['mobile']);
    $addr = $conn_rrpl -> real_escape_string(strtoupper($_POST['address']));
    $pincode = $conn_rrpl -> real_escape_string($_POST['pincode']);
    $hide = $conn_rrpl -> real_escape_string($_POST['hide']);
    $old_name = $conn_rrpl -> real_escape_string($_POST['check_consignor']);
    $consignor_name = $conn_rrpl -> real_escape_string(strtoupper($_POST['name']));
    $consignor_gst = $conn_rrpl -> real_escape_string(strtoupper($_POST['gst']));
     
   if(strlen($pincode)!=6)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Invaid Pincode $pincode !'
			})
			</script>"; 
			exit();
		} 
		
	$sql = $conn_rrpl->query("SELECT `id` FROM `consignor` WHERE `gst` = '$consignor_gst' and gst!='' AND `id` !='$id'");
  	if(mysqli_num_rows($sql) > 0)
  	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Duplicate GST No. $consignor_gst !'
		})
		</script>";	  
  	}
    else
    {  
		$sql = $conn_rrpl->query("SELECT `id` FROM `consignor` WHERE `name` = '$consignor_name' AND `id` !='$id'"); 
	  	if(mysqli_num_rows($sql) > 0)
	  	{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Duplicate Name $consignor_name !'
			})
			</script>"; 
	  	} else {

			try {
				$conn_rrpl->query("START TRANSACTION"); 

				$sql = "UPDATE `consignor` SET `name` = '$consignor_name', `gst` = '$consignor_gst', mobile = '$mobile', pincode = '$pincode', addr = '$addr', hide = '$hide' WHERE `id` = '$id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "UPDATE `lr_sample` SET `consignor`='$consignor_name',`con1_id`='$id',con1_gst='$consignor_gst' WHERE `con1_id` = '$id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "UPDATE `lr_sample` SET `consignor`='$consignor_name',`con1_id`='$id',con1_gst='$consignor_gst' WHERE con1_id='' 
				AND consignor='$old_name'";	 
				
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "UPDATE `freight_form_lr` SET `consignor` = '$consignor_name', `con1_id` = '$id' WHERE `con1_id`='$id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$sql = "UPDATE `freight_form_lr` SET `consignor`='$consignor_name',`con1_id`='$id' WHERE con1_id='' AND 
				consignor='$old_name'";	 
				
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}   

				$content = "Consignor Updated: ".$consignor_name." (".$consignor_gst.") & OldName: ".$old_name;
				$sql = "INSERT INTO `billing_log` (`content`, `timestamp`) VALUES ('$content','$sysdatetime')";
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$conn_rrpl->query("COMMIT");
				echo "
				<script>
				Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Consignor Updated.',
				showConfirmButton: false,
				timer: 1500
				})
				</script>";
			}
			catch(Exception $e) {
					$conn_rrpl->query("ROLLBACK"); 
					$content = $e->getMessage();
					$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
					echo "
					<script>
					Swal.fire({
					icon: 'error',
					title: 'Error !!!',
					text: '$content'
					})
					</script>";					
			} 
	  	} 
		 
	}
 
?>