<?php

require('connect.php');

	$group_name=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['group_name']));
	$mobile=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['group_mobile']));
	$addr=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['group_addr']));
 
	$sql = mysqli_query($conn_rrpl,"SELECT id FROM billing_group WHERE name='$group_name'");
	if(mysqli_num_rows($sql)>0)
	{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Duplicate Group Already Exists.'
			})
			</script>";	
	} else {
 
		$update=mysqli_query($conn_rrpl,"INSERT INTO billing_group (name, mobile, addr, timestamp) VALUES 
		('$group_name','$mobile','$addr','$sysdatetime')");
		
		if(!$update)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '".mysqli_error($conn_rrpl)."'
			})
			</script>";	   

		} else {

			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'New Group Updated.',
			showConfirmButton: false,
			timer: 1500
			})
			</script>";
		} 

	}