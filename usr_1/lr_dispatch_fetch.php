<?php
 
	require('connect.php');

    $daterange = $conn_rrpl -> real_escape_string($_POST['daterange']);
    $branch = $conn_rrpl -> real_escape_string($_POST['branch']);

	function before ($thiss, $inthat)
	{
	    return substr($inthat, 0, strpos($inthat, $thiss));
	}

	function after ($thiss, $inthat)
	{
	    if (!is_bool(strpos($inthat, $thiss)))
	        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
	}
	 
	$fromdate = before('-', $daterange); 
	$fromdate = strtotime($fromdate); 
	$todate = after('-', $daterange); 
	$todate = strtotime($todate);
 
					// <th>SystemDate</th>
					// <th>Company</th>
    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>FrtAcFor</th>
					<th>BillType</th>
					<th>Customer</th>
					<th>Consignee</th>
					<th>Consignee Name</th>
					<th>DespGroup</th> 
					<th>Company</th>
					<th>LRNO</th>
					<th>LRDate</th>
					<th>TruckNo</th>
					<th>FromStation</th>
					<th>ToStation</th>
					<th>DeleveryAt</th>
					<th>DCIP/DONo</th>
					<th>InvoiceNo</th>
					<th>ShipmentNo</th>
					<th>ActWt</th>
					<th>ChargeWt</th>
					<th>TRUCK TYPE</th>
					<th>QTY.</th>
					<th>Rate</th>
					<th>AMOUNT</th>
					<th>Item</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "lr_dispatch_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
 