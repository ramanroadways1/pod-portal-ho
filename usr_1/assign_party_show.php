<?php
	require('connect.php'); 
	$id =  $conn_rrpl -> real_escape_string($_POST['id']); 
  
    echo ' 
		<style type="text/css"> 
		.modal-backdrop
		{
		opacity:0.5 !important;
		}

		.modal-lg{
		max-width: 1200px;
		}
 	
 		#user_data_2_paginate{
			margin-bottom: 20px;
		}
		</style> 
    	<div class="modal-body" style="margin: 10px;"> 
		  	<table id="user_data_2" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>Sno</th>
					<th>LR_No</th>
					<th>Branch</th>
					<th>Date</th>
					<th>TruckNo</th>
					<th>From</th>
					<th>To</th>
					<th>Consignee</th>
					<th>Weight</th>
					<th>Item</th>
				</tr>
		      </thead> 
		 	</table>
		</div>

		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data_2").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "assign_party_show2.php?p='.$id.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		// "dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" } 
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data_2").DataTable(); 
		} ); </script>';
 
?>  


 
<?php
mysqli_close($conn_rrpl);
?> 