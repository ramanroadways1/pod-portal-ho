<?php require('header.php');


$sql = "select count(id) as cnt from consignor where approval='0'";
$res = $conn_rrpl->query($sql);
$row = $res->fetch_assoc();
$card1 = $row['cnt'];

$sql = "select count(id) as cnt from consignor where bill_party_id='' or bill_party_id='0'";
$res = $conn_rrpl->query($sql);
$row = $res->fetch_assoc();
$card2 = $row['cnt'];

$sql = "select count(id) as cnt from rcv_pod where billing!='1' and datediff(curdate(),pod_date) > 3";
$res = $conn_rrpl->query($sql);
$row = $res->fetch_assoc();
$card3 = $row['cnt'];

$sql = "select count(id) as cnt from podmemo where sentby='courier' and docketno=''";
$res = $conn_rrpl->query($sql);
$row = $res->fetch_assoc();
$card4 = $row['cnt'];

 ?>
        <div class="row">
          <div class="col-md-6">
                    <div class="row">
<style type="text/css">
  .card-stats{
    border-radius: 0px;
  }
</style>
          <div class="col-md-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-single-02 text-warning"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category"> Consignor Approval</p>
                      <p class="card-title"> <?= $card1; ?>
                        <p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
              </div>
            </div>
          </div>
          <div class="col-md-6  ">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-money-coins text-success"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">   Pending  Party  Assign</p>
                      <p class="card-title"><?= $card2; ?>
                        <p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-paper text-danger"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">POD Dispatch Pending</p>
                      <p class="card-title"> <?= $card3; ?>
                        <p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-app text-primary"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Pending Docket No</p>
                      <p class="card-title"> <?= $card4; ?>
                        <p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
              </div>
            </div>
          </div> 
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
                <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row"> 
                      <div class="col-md-12">
                        <div class="numbers">
    <form method="post" action="show_vou.php" autocomplete="off" target="_blank">
                            <p class="card-category" style="text-align: left; margin-bottom: 7px; font-size: 16px;"> Truck Voucher  No </p>
                              <p class="card-title">
                                <div class="row">                                
                                <div class="col-md-9">
 <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="vou_no" required />
         <input value="Truck_Voucher" type="hidden" name="voutype" />                                </div>
                                <div class="col-md-2" style="padding: 0px;"> 
                                  <button  type="submit" name="submit" style="margin: 0px; font-size:17px; vertical-align: top; " class="btn btn-sm btn-warning"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </div> 
                                </div>
                              <p>
    </form> 
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer " style="padding: 5.1px;">
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row"> 
                      <div class="col-md-12">
                        <div class="numbers">
    <form method="post" action="show_vou.php" autocomplete="off" target="_blank">
                            <p class="card-category" style="text-align: left; margin-bottom: 7px; font-size: 16px;"> Expense Voucher  No </p>
                              <p class="card-title">
                                <div class="row">                                
                                <div class="col-md-9">
 <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="vou_no" required />
         <input value="Expense_Voucher" type="hidden" name="voutype" />                                </div>
                                <div class="col-md-2" style="padding: 0px;"> 
                                  <button style="margin: 0px; font-size:17px; vertical-align: top; " class="btn btn-sm btn-warning"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </div> 
                                </div>
                              <p>
    </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer " style="padding: 5.1px;">
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row"> 
                      <div class="col-md-12">
                        <div class="numbers">
    <form method="post" action="show_fm.php" autocomplete="off" target="_blank">
                            <p class="card-category" style="text-align: left; margin-bottom: 7px; font-size: 16px;"> Freight Memo No </p>
                              <p class="card-title">
                                <div class="row">                                
                                <div class="col-md-9">
<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="idmemo" required />
         <input value="FM" type="hidden" name="key" />                                </div>
                                <div class="col-md-2" style="padding: 0px;"> 
                                  <button style="margin: 0px; font-size:17px; vertical-align: top; " class="btn btn-sm btn-warning"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </div> 
                                </div>
                              <p>
    </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer " style="padding: 5.1px;">
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row"> 
                      <div class="col-md-12">
                        <div class="numbers">
    <form method="post" action="show_fm.php" autocomplete="off" target="_blank">
                            <p class="card-category" style="text-align: left; margin-bottom: 7px; font-size: 16px;"> LR No </p>
                              <p class="card-title">
                                <div class="row">                                
                                <div class="col-md-9">
 <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="lrno" required />
         <input value="LR" type="hidden" name="key" />                                </div>
                                <div class="col-md-2" style="padding: 0px;"> 
                                  <button style="margin: 0px; font-size:17px; vertical-align: top; " class="btn btn-sm btn-warning"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </div> 
                                </div>
                              <p>
    </form> 
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer " style="padding: 5.1px;">
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row"> 
                      <div class="col-md-12">
                        <div class="numbers">
    <form method="post" action="tdv_by_tno.php" autocomplete="off" target="_blank">
                            <p class="card-category" style="text-align: left; margin-bottom: 7px; font-size: 16px;">  Voucher's by Truck No </p>
                              <p class="card-title">
                                <div class="row">                                
                                <div class="col-md-9">
         <input id="tno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="tno" required />
                                </div>
                                <div class="col-md-2" style="padding: 0px;"> 
                                  <button style="margin: 0px; font-size:17px; vertical-align: top; " class="btn btn-sm btn-warning"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </div> 
                                </div>
                              <p>
    </form> 
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer " style="padding: 5.1px;">
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row"> 
                      <div class="col-md-12">
                        <div class="numbers">
    <form method="post" action="report_track_index.php" autocomplete="off" target="_blank">
                            <p class="card-category" style="text-align: left; margin-bottom: 7px; font-size: 16px;"> Courier Tracking</p>
                              <p class="card-title">
                                <div class="row">                                
                                <div class="col-md-9">
                                <input type="" class="form-control" name="p" required="">
                                </div>
                                <div class="col-md-2" style="padding: 0px;"> 
                                  <button style="margin: 0px; font-size:17px; vertical-align: top; " class="btn btn-sm btn-warning"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </div> 
                                </div>
                              <p>
    </form>  
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer " style="padding: 5.1px;">
                  </div>
                </div>
                </div>
          </div>
        </div>
        </div>

<div class="row">
  <div class="col-md-12">
    
  </div>
</div>

        <div class="row" style="display: none;">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Users Behavior</h5>
                <p class="card-category">24 Hours performance</p>
              </div>
              <div class="card-body ">
                <canvas id=chartHours width="400" height="100"></canvas>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Updated 3 minutes ago
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row"  style="display: none;">
          <div class="col-md-4">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Email Statistics</h5>
                <p class="card-category">Last Campaign Performance</p>
              </div>
              <div class="card-body ">
                <canvas id="chartEmail"></canvas>
              </div>
              <div class="card-footer ">
                <div class="legend">
                  <i class="fa fa-circle text-primary"></i> Opened
                  <i class="fa fa-circle text-warning"></i> Read
                  <i class="fa fa-circle text-danger"></i> Deleted
                  <i class="fa fa-circle text-gray"></i> Unopened
                </div>
                <hr>
                <div class="stats">
                  <i class="fa fa-calendar"></i> Number of emails sent
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8"  style="display: none;">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-title">NASDAQ: AAPL</h5>
                <p class="card-category">Line Chart with Points</p>
              </div>
              <div class="card-body">
                <canvas id="speedChart" width="400" height="100"></canvas>
              </div>
              <div class="card-footer">
                <div class="chart-legend">
                  <i class="fa fa-circle text-info"></i> Tesla Model S
                  <i class="fa fa-circle text-warning"></i> BMW 5 Series
                </div>
                <hr/>
                <div class="card-stats">
                  <i class="fa fa-check"></i> Data information certified
                </div>
              </div>
            </div>
          </div>
        </div>
<?php include('footer.php'); ?>