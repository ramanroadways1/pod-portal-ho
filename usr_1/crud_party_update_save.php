<?php

require('connect.php');

	$id=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['id']));
	$name=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['name']));
	$branch=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['branch']));
	$gst_no=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['gst_no']));
	$pan_no=substr($gst_no,2,10);
	$email=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['email']));
	$mobile_no=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['mobile_no']));
	$payment_terms=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['payment_terms']));
	$rate_frq=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['rate_frq']));
	$rate_change_date=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['rate_change_date']));
	$addr=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['addr']));
	$emd=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['emd']));
	$incharge=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['incharge']));
	$incharge_mobile=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['incharge_mobile']));
	$con_start=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['con_start']));
	$con_end=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['con_end']));
	  
	$update=mysqli_query($conn_rrpl,"UPDATE billing_party SET name='$name', bill_branch='$branch', gst_no='$gst_no', payment_term='$payment_terms', rate_fq='$rate_frq', rate_change_date='$rate_change_date', billing_addr='$addr', email_id='$email', mobile='$mobile_no', pan_no='$pan_no', emd='$emd', incharge_name='$incharge', incharge_mobile='$incharge_mobile', contract_from='$con_start', contract_to='$con_end' WHERE id='$id'");
	
	if(!$update)
	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: '".mysqli_error($conn_rrpl)."'
		})
		</script>";	   

	} else {

		echo "
		<script>
		Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'Party Updated.',
		showConfirmButton: false,
		timer: 1500
		})
		</script>";
	} 