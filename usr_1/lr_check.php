<?php require('header.php'); ?>
  
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<style type="text/css">
	.applyBtn{
		border-radius: 0px !important;
	}
	.show-calendar{
		top: 180px !important;
	} 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
     .dt-buttons{float: right;}
    .user_data_filter{
        float: right;
    }

    .dt-button {
        padding: 5px 20px;
        text-transform: uppercase;
        font-size: 12px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 4px #999;
    }

    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }


#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444;} .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
  .table .thead-light th{
    text-transform: none !important;
  } 
</style>
<script type="text/javascript"> 

  $(function() {
    // $("#lrno").autocomplete({
    // source: 'reports_index_auto.php',
    // appendTo: '#appenddiv2',
    // select: function (event, ui) { 
    //            $('#lrno').val(ui.item.value);   
    //            $('#lrnoid').val(ui.item.dbid);      
    //            return false;
    // },
    // change: function (event, ui) {
    // if(!ui.item){
    //     $(event.target).val("");
    //   Swal.fire({
    //   icon: 'error',
    //   title: 'Error !!!',
    //   text: 'LR does not exists !'
    //   })
    //   $("#lrnoid").val("");
    //   $("#lrno").val("");
    //   $("#lrno").focus();
    // }
    // }, 
    // focus: function (event, ui){
    // return false;
    // }
    // });
  });

</script>
 <div class="col-md-12"> <h3>Check LR's</h3> </div>
  
 <div id="response"></div>
  
<form method="post" action="" id="getPAGE" autocomplete="off"> 
<div class="col-md-12" >
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">
  <div class="row">
 
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style type="text/css">
      .show-calendar{
        top: 250px !important;
      }
    </style>

<div class="col-md-3">
      <label style="text-transform: uppercase;"> Upload Excel (LRNO's) </label>
  <input type="file" required="" name="form-control" style="border: 1px solid #ccc;">
</div>
    <!-- <div class="col-md-2">
      <label style="text-transform: uppercase;"> BRANCH </label>
      <select class="form-control" name="branch" id="branch" required="">
        <option value=""> -- select -- </option>
        <option value="ALL"> ALL BRANCH </option>
        <?php
          // $sql = "select username from user where role='2' order by username asc";
          // $res = $conn_rrpl->query($sql);
          // while($roo = $res->fetch_assoc()){
          //   echo "<option value='".$roo['username']."'> ".$roo['username']." </option>";
          // } 
        ?> 
      </select>
    </div>  
    <div class="col-md-4">
      <label style="text-transform: uppercase;">DATE RANGE</label>
      <input type="text" name="daterange" class="form-control" value="" required="required" />
      <input type="hidden" id="fromdate" name="fromdate">
      <input type="hidden" id="todate" name="todate"> 
    </div>
   -->
 
<script type="text/javascript">
  $(function() {
      $('input[name="daterange"]').daterangepicker({
        // minDate: '2019-09-15',
        opens: 'left'
      }, function(start, end, label) {
        document.getElementById('fromdate').value=start.format('YYYY-MM-DD');
        document.getElementById('todate').value=end.format('YYYY-MM-DD'); 
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      });
  });
</script>

    <div class="col-md-1" style="top:-5px !important;"> 
      <label style=""> </label> <br>
      <button type="submit" class='btn btn-success'> <i class='fa fa-search '></i> <b>  </b> </button>    
      
    </div>
  </div>
</div>
</form> 
</div>

<div id="getPAGEDIV" class="col-md-12" style="margin-top: 25px !important;"></div>


<script type="text/javascript">
  function updt(sel) { 
      // if(sel.value == "brancho" || sel.value == "branchi")
      // {
      //   $("#branch").prop('required',true);
      //   $("#branch").prop('disabled',false);
      // } else {
      //   $("#branch").prop('required',false);
      //   $("#branch").prop('disabled',true);
      // }  
      //   if(sel.value == "lrno") 
      //   {
      //     $("#lrno").prop('required',true);
      //     $("#lrno").prop('disabled',false);
      //   } else {
      //     $("#lrno").prop('required',false);
      //     $("#lrno").prop('disabled',true);
      //   }
  } 
 
  $(document).on('submit', '#getPAGE', function()
    {   
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'lr_cehck2.php',
        data : data,
        success: function(data) {       
        $('#getPAGEDIV').html(data);  
        }
      });
      return false;  
   });
</script>
 <?php include('footer.php'); ?>