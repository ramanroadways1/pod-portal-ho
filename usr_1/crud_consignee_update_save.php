<?php 

	require('connect.php'); 

    $id = $conn_rrpl -> real_escape_string($_POST['id']);
    $code = $conn_rrpl -> real_escape_string($_POST['code']);
    $consignee_name = $conn_rrpl -> real_escape_string(strtoupper($_POST['name']));
    $consignee_gst = $conn_rrpl -> real_escape_string(strtoupper($_POST['gst']));
    $mobile = $conn_rrpl -> real_escape_string($_POST['mobile']);
    $addr = $conn_rrpl -> real_escape_string(strtoupper($_POST['address']));
    $pincode = $conn_rrpl -> real_escape_string($_POST['pincode']);
    $hide = $conn_rrpl -> real_escape_string($_POST['hide']); 
    $old_name = $conn_rrpl -> real_escape_string($_POST['check_consignee']); 
 
   if(strlen($pincode)!=6)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Invaid Pincode $pincode !'
			})
			</script>"; 
			exit();
		} 
	
 	$sql = $conn_rrpl->query("SELECT `id` FROM `consignee` WHERE `gst` = '$consignee_gst' and gst!='' AND `id` !='$id'");
  	if(mysqli_num_rows($sql) > 0)
  	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Duplicate GST No. $consignee_gst !'
		})
		</script>";	  
  	}
    else
    {  
		$sql = $conn_rrpl->query("SELECT `id` FROM `consignee` WHERE `name` = '$consignee_name' AND `id` !='$id'"); 
	  	if(mysqli_num_rows($sql) > 0)
	  	{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Duplicate Name $consignee_name !'
			})
			</script>"; 
	  	} else {

			try {
				$conn_rrpl->query("START TRANSACTION"); 

				$sql = "UPDATE `consignee` SET `name` = '$consignee_name', `gst` = '$consignee_gst', mobile = '$mobile', pincode = '$pincode', addr = '$addr', hide = '$hide' WHERE `id` = '$id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "UPDATE `lr_sample` SET `consignee`='$consignee_name',`con2_id`='$id',con2_gst='$consignee_gst' WHERE `con2_id` = '$id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "UPDATE `lr_sample` SET `consignee`='$consignee_name',`con2_id`='$id',con2_gst='$consignee_gst' WHERE con2_id='' AND consignee='$old_name'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "UPDATE `freight_form_lr` SET `consignee` = '$consignee_name', `con2_id` = '$id' WHERE `con2_id`='$id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$sql = "UPDATE `freight_form_lr` SET `consignee`='$consignee_name',`con2_id`='$id' WHERE con2_id='' AND consignee='$old_name'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}   

				$content = "Consignee Updated: ".$consignee_name." (".$consignee_gst.") & OldName: ".$old_name;
				$sql = "INSERT INTO `billing_log` (`content`, `timestamp`) VALUES ('$content','$sysdatetime')";
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$conn_rrpl->query("COMMIT");
				echo "
				<script>
				Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Consignee Updated.',
				showConfirmButton: false,
				timer: 1500
				})
				</script>";
			}
			catch(Exception $e) {
					$conn_rrpl->query("ROLLBACK"); 
					$content = $e->getMessage();
					$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
					echo "
					<script>
					Swal.fire({
					icon: 'error',
					title: 'Error !!!',
					text: '$content'
					})
					</script>";					
			} 
	  	} 
		 
	}

  
?> 