<?php

  require('connect.php');
  
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
 
      $statement = $connection->prepare("SELECT * FROM billing_party");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
  $sub_array[] = " <center> <button onclick='update(".$row['id'].")' class='btn btn-sm btn-warning'> <i class='fa fa-edit '></i> <b>UPDATE</b> </button> </center> "; 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['id']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['name']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['bill_branch']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['gst_no']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['email_id']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['mobile']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['billing_addr']);

  $sub_array[] = $conn_rrpl -> real_escape_string($row['incharge_name']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['incharge_mobile']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['payment_term']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['rate_fq']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['rate_change_date']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['emd']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['contract_from']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['contract_to']); 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['timestamp']); 

  $data[] = $sub_array;

} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>