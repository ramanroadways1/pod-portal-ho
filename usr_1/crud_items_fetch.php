<?php

  require('connect.php');
  
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
      $statement = $connection->prepare("SELECT * FROM lritems");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
  $sub_array[] = "<center> <button disabled onclick='update(".$row['id'].")' class='btn btn-sm btn-warning'> <i class='fa fa-edit '></i> <b> UPDATE ITEM </b> </button> </center>"; 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['icode']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['igroup']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['iname']);
  $data[] = $sub_array;
} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>