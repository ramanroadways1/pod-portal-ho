<?php

  require('connect.php');
  
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
      $statement = $connection->prepare("SELECT * FROM billing_group");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
  $sub_array[] = " <center> 
   <button onclick='con(".$row['id'].")' class='btn btn-sm btn-warning'> <i class='fa fa-edit '></i> <b>UPDATE CONSIGNOR</b> </button>  &nbsp; <button onclick='party(".$row['id'].")' class='btn btn-sm btn-success'> <i class='fa fa-edit '></i> <b>ASSIGN PARTY</b> </button> &nbsp;
   <button onclick='dele(".$row['id'].")' class='btn btn-sm btn-danger'> <i class='fa fa-ban '></i> <b>DELETE</b> </button> 
   </center>"; 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['id']);
  $sub_array[] = $conn_rrpl -> real_escape_string($row['name']);

    $billparty = $conn_rrpl -> real_escape_string($row['party']); 
    $fields = array();

    if($billparty!='0' && $billparty!=''){
       $groupi = explode(",", $billparty); 
       $groupi_count = count($groupi);

        if($groupi_count > 1){
          for($i=0;$i<$groupi_count;$i++) { 
            $query = $conn_rrpl->query("Select * From billing_party where id='$groupi[$i]'");
            $row3 = $query->fetch_assoc(); 
            ${'prty_'.$i} = ucwords(strtolower($row3['name']));
            array_push($fields, ${'prty_'.$i});  
          }
          $getprty = implode(", ", $fields);
        } else { 
            $query = $conn_rrpl->query("Select * From billing_party where id='$billparty'");
            $row4 = $query->fetch_assoc(); 
            $getprty = ucwords(strtolower($row4['name']));
        } 
    } else {
      $getprty = "NA";
    }
 
  $sub_array[] = "<center>".$getprty."</center>";

    $consignor = $conn_rrpl -> real_escape_string($row['consignor']); 
    $fields2 = array();

    if($consignor!='0' && $consignor!=''){
       $groupi = explode(",", $consignor); 
       $groupi_count = count($groupi);

        if($groupi_count > 1){
          for($i=0;$i<$groupi_count;$i++) { 
            $query = $conn_rrpl->query("Select * From consignor where id='$groupi[$i]'");
            $row3 = $query->fetch_assoc(); 
            ${'prty_'.$i} = ucwords(strtolower($row3['name']));
            array_push($fields2, ${'prty_'.$i});  
          }
          $getcon = implode(", ", $fields2);
        } else { 
            $query = $conn_rrpl->query("Select * From consignor where id='$consignor'");
            $row4 = $query->fetch_assoc(); 
            $getcon = ucwords(strtolower($row4['name']));
        } 
    } else {
      $getcon = "NA";
    }

  $sub_array[] = $getcon; 

  $data[] = $sub_array;

} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>