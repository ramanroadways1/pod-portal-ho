<?php

require('connect.php');

 	$name=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['name']));
	$branch=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['branch']));
	$gst_no=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['gst_no']));
	$pan_no=substr($gst_no,2,10);
	$email=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['email']));
	$mobile_no=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['mobile_no']));
	$payment_terms=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['payment_terms']));
	$rate_frq=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['rate_frq']));
	$rate_change_date=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['rate_change_date']));
	$addr=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['addr']));
	$emd=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['emd']));
	$incharge=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['incharge']));
	$incharge_mobile=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['incharge_mobile']));
	$con_start=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['con_start']));
	$con_end=mysqli_real_escape_string($conn_rrpl,strtoupper($_POST['con_end']));
	  
	$sql = mysqli_query($conn_rrpl,"SELECT id FROM billing_party WHERE name='$name' or (gst_no='$gst_no' and gst_no!='')");
	if(mysqli_num_rows($sql)>0)
	{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Duplicate Party Already Exists.'
			})
			</script>";	
	} else {

		$update=mysqli_query($conn_rrpl,"INSERT INTO billing_party (name, bill_branch, gst_no, payment_term, rate_fq, rate_change_date, billing_addr, email_id, mobile, pan_no, emd, incharge_name, incharge_mobile, contract_from, contract_to, timestamp) VALUES ('$name', '$branch','$gst_no','$payment_terms','$rate_frq','$rate_change_date','$addr','$email','$mobile_no', '$pan_no', '$emd','$incharge','$incharge_mobile','$con_start','$con_end','$sysdatetime')");
	
		if(!$update)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '".mysqli_error($conn_rrpl)."'
			})
			</script>";	   

		} else {

			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'New Party Updated.',
			showConfirmButton: false,
			timer: 1500
			})
			</script>";
		} 

	}