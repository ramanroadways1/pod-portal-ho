<?php
	require('connect.php'); 
	$id =  $conn_rrpl -> real_escape_string($_POST['id']);
  
    $members = $conn_rrpl->query("SELECT * FROM `consignor` where id=$id");
    $mem = mysqli_fetch_assoc($members); 
?> 
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}

#appenddiv {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
</style>
<script src="../assets/bundle.min.js"></script> 
<form method="post" action="" id="addonreq" role="form" autocomplete="off">
		<div class="modal-body">
			<p style="color: #444;"> ASSIGN ADDON PARTY  <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
			</p> 
			<div class="row">

				<div class="form-group col-md-3">
						<label for="phone">CONSIGNOR CODE</label>
						<input type="text" class="form-control" name="" value="<?php echo $mem['code'];?>" readonly /> 
						<input type="hidden" name="coid" value="<?php echo $mem['id'];?>" /> 
				</div> 

		 		<div class="form-group col-md-6">
						<label for="phone">CONSIGNOR NAME </label>
						<input type="text" class="form-control" name="" value="<?php echo $mem['name'];?>" readonly />
						<input type="hidden" name="" value="<?php echo $mem['id'];?>" readonly />
				</div>  

				<div class="form-group col-md-3">
						<label for="phone">CONSIGNOR GST</label>
						<input type="text" class="form-control" name="" value="<?php echo $mem['gst'];?>" readonly /> 
				</div>  
 
				<div class="form-group col-md-12" id="">
						<label for="phone">BILLING PARTY </label>

						<div class="select-wrapper"> 
    					<span class="autocomplete-select"></span>
    					</div>
    					<input type="hidden" name="allparty" id="allparty" value="<?php echo $mem['addon_party']; ?>">
  
					    <script> 
					      var autocomplete = new SelectPure(".autocomplete-select", {
					        options: [
					        <?php
					         $query = $conn_rrpl->query("Select * From billing_party");
					         while ($row = $query->fetch_assoc()) 
					          { echo '{ label: "'.ucwords(strtolower($row['name'])).'", value: "'.$row['id'].'", },'; }  
					          ?>
					        ], 
					        multiple: true,
					        autocomplete: true,
					        icon: "fa fa-times",
					        placeholder: "-Please select-", //"5", "8"
					        value: [<?php
					        $billparty = $mem['addon_party']; 
					         if($billparty!='0' && $billparty!=''){
						     $groupi = explode(",", $billparty); 
						     $groupi_count = count($groupi);
 
						      if($groupi_count > 1){
						      	for($i=0;$i<$groupi_count;$i++) {
						      	echo '"'.$groupi[$i].'"';
						      		if($i==$groupi_count-1){
						      			echo "";
						      		} else {
						      			echo ",";
						      		}
						      	}
						      } else {
						      	echo '"'.$groupi[0].'"';
						      } 
					         } ?>],
					        onChange: value => { console.log(value); 
					        document.getElementById('allparty').value = value; },
					      }); 
					    </script> 
				</div> 
		   </div> 
		</div>
		<div class="modal-footer">
			<button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
			<input type="submit" id="updatereqbtn" class="btn btn-primary" name="submit" value="UPDATE" />
		</div>
</form> 
 
     <style>  
      .select-wrapper {
        margin: auto;
        max-width: auto;
        width: auto;
      }

      .select-pure__select {
        align-items: center;
        background: #f9f9f8;
        border-radius: 4px;
        border: 1px solid rgba(0, 0, 0, 0.15);
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.04);
        box-sizing: border-box;
        color: #363b3e;
        cursor: pointer;
        display: flex;
        font-size: 16px;
        font-weight: 500;
        justify-content: left;
        min-height: 44px;
        padding: 5px 10px;
        position: relative;
        transition: 0.2s;
        width: 100%;
      }

      .select-pure__options {
        border-radius: 4px;
        border: 1px solid rgba(0, 0, 0, 0.15);
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.04);
        box-sizing: border-box;
        color: #363b3e;
        display: none;
        left: 0;
        max-height: 221px;
        overflow-y: scroll;
        position: absolute;
        top: 50px;
        width: 100%;
        z-index: 5;
      }

      .select-pure__select--opened .select-pure__options {
        display: block;
      }

      .select-pure__option {
        background: #fff;
        border-bottom: 1px solid #e4e4e4;
        box-sizing: border-box;
        height: 44px;
        line-height: 25px;
        padding: 10px;
      }

      .select-pure__option--selected {
        color: #e4e4e4;
        cursor: initial;
        pointer-events: none;
      }

      .select-pure__option--hidden {
        display: none;
      }

      .select-pure__selected-label {
        align-items: 'center';
        background: #5e6264;
        border-radius: 4px;
        color: #fff;
        cursor: initial;
        display: inline-flex;
        justify-content: 'center';
        margin: 5px 10px 5px 0;
        padding: 3px 7px;
      }

      .select-pure__selected-label:last-of-type {
        margin-right: 0;
      }

      .select-pure__selected-label i {
        cursor: pointer;
        display: inline-block;
        margin-left: 7px;
      }

      .select-pure__selected-label img {
        cursor: pointer;
        display: inline-block;
        height: 18px;
        margin-left: 7px;
        width: 14px;
      }

      .select-pure__selected-label i:hover {
        color: #e4e4e4;
      }

      .select-pure__autocomplete {
        background: #f9f9f8;
        border-bottom: 1px solid #e4e4e4;
        border-left: none;
        border-right: none;
        border-top: none;
        box-sizing: border-box;
        font-size: 16px;
        outline: none;
        padding: 10px;
        width: 100%;
      }

      .select-pure__placeholder--hidden {
        display: none;
      }
    </style>
<?php
mysqli_close($conn_rrpl);
?> 