<?php

  require 'connect.php';

  $consignee_name = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['name']));
  $consignee_gst = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['gst']));
  $mobile = mysqli_real_escape_string($conn_rrpl, ($_POST['mobile']));
  $addr = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['address']));
  $pincode = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['pincode']));

   // if($pincode!=6)
   if(strlen($pincode)!=6)
   {

   	echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Invalid Pincode $pincode !'
		})
		</script>";	  
		exit();
  	}  
	
  $sql = mysqli_query($conn_rrpl, "SELECT `gst` FROM `consignee` WHERE `gst` = '$consignee_gst' AND gst!=''");
   if (mysqli_num_rows($sql) > 0)
   {

   	echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Duplicate GST No. $consignee_gst !'
		})
		</script>";	  
  	}  
   else
   { 
		$sql = mysqli_query($conn_rrpl, "SELECT `gst` FROM `consignee` WHERE `name` = '$consignee_name'");
		if(mysqli_num_rows($sql) > 0)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Duplicate Name $consignee_name !'
			})
			</script>"; 
		} 
		else
		{
			$sql = mysqli_query($conn_rrpl, "INSERT INTO `consignee`(branch, `name`, `gst`, mobile, addr, pincode) VALUES ('SALES', '$consignee_name','$consignee_gst','$mobile', '$addr', '$pincode')");
			 
			 $insert_id = mysqli_insert_id($conn_rrpl);
			 
			 $update = mysqli_query($conn_rrpl,"UPDATE consignee SET code='$insert_id' WHERE id='$insert_id'");
			 
			 if ($sql) 
			  {
					echo "
					<script>
					Swal.fire({
					position: 'top-end',
					icon: 'success',
					title: 'New Consignee Updated.',
					showConfirmButton: false,
					timer: 1500
					})
					</script>"; 
			  }
			  else
			  { 
				echo "
				<script>
				Swal.fire({
				icon: 'error',
				title: 'Error !!!',
				text: '".mysqli_error($conn_rrpl)."'
				})
				</script>";  
			  }
		}

	}
?>