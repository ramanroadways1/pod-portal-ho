<?php require('connect.php'); ?>

<!DOCTYPE html>
<html lang="en"> 
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  RAMAN ROADWAYS Pvt. Ltd.
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />

  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>

  <link rel="stylesheet" href="../assets/jquery-ui.min.css" type="text/css" />     
  <script type="text/javascript" src="../assets/jquery-ui.min.js"></script>  
  <script src="../assets/jquery.dataTables.min.js"></script>  

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<style type="text/css">
  input[required], select[required] {
    background-image: url('../assets/qD0jR.png');
    background-repeat: no-repeat;
    background-position-x: right;
  }
  label{color: #5a5a5a;}
  input{
    text-transform: uppercase;
  }
</style>

<body class="" >
 
  <div id="loadicon" style="display:none; position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#ffffff; z-index: 30001; opacity:0.8; cursor: wait;">
  <center><img src="../assets/loader.gif" style="margin-top:50px;" /> </center>
  </div>

  <div class="wrapper "> 
    <style type="text/css">
      .main-panel{
        width: calc(100%) !important;
      }

      .navbar.navbar-transparent{
        background-color: #fff !important;
      }
    </style>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent" >
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="index.php">
            <i class="fa fa-truck" aria-hidden="true"></i> RAMAN_ROADWAYS
            </a>


          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
             
            <ul class="navbar-nav">
               

               <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<i class="fa fa-area-chart" aria-hidden="true"></i> Reports
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink"> 
                <a class="dropdown-item" href="lr_dispatch.php">LR Dispatch  </a>  
                  <a class="dropdown-item" href="truck_freight.php"> Own Truck Freight </a>  
                  <a class="dropdown-item" href="market_bilty.php"> Market Bilty </a>  
                  <a class="dropdown-item" href="lr_check.php"> Check LR's  </a>  
                  <a class="dropdown-item" href="truck_lr.php"> Truck Wise LR </a>  
                  <a class="dropdown-item" href="cashbook_index.php"> Cash Book </a>  
                </div>
              </li>    

               <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<i class="fa fa-edit" aria-hidden="true"></i>  MASTER 
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="crud_consignor.php">Consignor</a> 
                  <a class="dropdown-item" href="crud_consignee.php">Consignee</a> 
                  <a class="dropdown-item" href="crud_group.php"> Groups</a> 
                  <a class="dropdown-item" href="crud_party.php"> Billing Party</a> 
                  <a class="dropdown-item" href="crud_items.php"> Items</a> 
              </div>
              </li>


              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<i class="fa fa-list-alt" aria-hidden="true"></i> Approval
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">

                  <a class="dropdown-item" href="assign_party.php">Assign Biiling Party </a> 
                  <a class="dropdown-item" href="approv_con_index.php">Consignor Approval </a> 
                  <a class="dropdown-item" href="showhide_consignor.php">Show/Hide Consignor</a> 
                  <a class="dropdown-item" href="showhide_items.php">Show/Hide Items</a> 
                </div>
              </li>    


              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<i class="fa fa-file-o" aria-hidden="true"></i> POD
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink"> 
                <a class="dropdown-item" href="reports_index.php"> Dispatch Report  </a>  
                  <a class="dropdown-item" href="pending_report.php"> Pending POD </a>  
                  <a class="dropdown-item" href="report_track_index.php"> Track Shipment </a>  
                  <a class="dropdown-item" href="pending_doc.php"> Pending Docket </a>  
                  <a class="dropdown-item" href="pod_update.php"> Update POD </a>  
                </div>
              </li>    

              <li class="nav-item">
                <a class="nav-link btn-rotate" href="logout.php">
<i class="fa fa-user-o" aria-hidden="true"></i> Log Out
                  <p>
                    <span class="d-lg-none d-md-block">Account</span>
                  </p>
                </a>
              </li> 
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-lg">

  <canvas id="bigDashboardChart"></canvas>


</div> -->
      <div class="content" >
        