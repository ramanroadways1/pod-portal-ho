<?php

	require 'connect.php';
 
	$beforename = $conn_rrpl -> real_escape_string(strtoupper($_POST['beforename'])); 
	$aftername = $conn_rrpl -> real_escape_string(strtoupper($_POST['aftername'])); 
	$beforeid = $conn_rrpl -> real_escape_string($_POST['beforeid']); 
	$afterid = $conn_rrpl -> real_escape_string($_POST['afterid']);  
 
	$con2_id_old = mysqli_query($conn_rrpl,"SELECT id FROM consignee WHERE id='$beforeid'");
	$con2_id_new = mysqli_query($conn_rrpl,"SELECT id, gst FROM consignee WHERE id='$afterid'"); 
	$row_old = mysqli_fetch_array($con2_id_old);
	$row_new = mysqli_fetch_array($con2_id_new);
  
	$old_id = $row_old['id'];
	$new_id = $row_new['id'];
	$new_gst = $row_new['gst'];
  
	if($old_id=='' || $new_id=='') 
	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Unable to Identify Consignee !'
		})
		</script>";	  

	} else {

		if($old_id!=$new_id)
 		{ 
 				try {
				$conn_rrpl->query("START TRANSACTION"); 

				$sql = "UPDATE consignee SET hide='1' WHERE id='$old_id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "update lr_sample set consignee='$aftername',con2_id='$new_id',con2_gst='$new_gst' where con2_id='$old_id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "update lr_sample set consignee='$aftername',con2_id='$new_id',con2_gst='$new_gst' where con2_id='' 
				AND consignee='$beforename'";
				
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "update freight_form_lr set consignee='$aftername', con2_id='$new_id' where con2_id='$old_id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$sql = "update freight_form_lr set consignee='$aftername',con2_id='$new_id' where con2_id='' AND consignee='$beforename'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}   

				$content = "consignee Merged: ".$beforename." to ".$aftername;
				$sql = "INSERT INTO `billing_log` (`content`,`timestamp`) VALUES ('$content','$sysdatetime')";
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$conn_rrpl->query("COMMIT");
				echo "
				<script>
				Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Consignee Updated.',
				showConfirmButton: false,
				timer: 1500
				})
				</script>";
			}
			catch(Exception $e) {
					$conn_rrpl->query("ROLLBACK"); 
					$content = $e->getMessage();
					$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
					echo "
					<script>
					Swal.fire({
					icon: 'error',
					title: 'Error !!!',
					text: '$content'
					})
					</script>";					
			}  
	 	} 
	 	else
	 	{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Consignee Can\'t be Same'
			})
			</script>";	  
	 	}

	} 

?>