<?php

  require('connect.php');
  
$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
$statement = $connection->prepare("SELECT branch, GROUP_CONCAT(lrno) as lrno, COUNT(*) as count FROM rcv_pod where billing!='1' and self='0' and nullify='0' and datediff(curdate(),exdate) > 3 GROUP by branch");
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array(); 
  
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = "<center>".$row["branch"]."</center>"; 
  $sub_array[] = "<center>".$row["count"]."</center>"; 
  $sub_array[] = $row["lrno"];  
 
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 