<?php
	require('connect.php');  
?>

<script type="text/javascript">
function gst(gst){
    var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
    
    if (gstinformat.test(gst)) {
     return true;
    } else {
 		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Please Enter Valid GSTIN Number !'
		}) 
		$("#gst_no").val('');
        $("#gst_no").focus();
    }
    
}
</script>	
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}
</style>
<form method="post" action="" id="addreq" role="form" autocomplete="off">
	<div class="modal-body">
<p style="color: #444;"> ADD NEW BILLING PARTY  <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
		</p>

		
		<div class="row">
 		  			<div class="form-group col-md-4">
                      <label for="consignor_name">Party Name </label>
					  <input type="text" name="name" class="form-control" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')" required> 
                    </div>
					 
 					<div class="form-group col-md-4">
						<label for="consignor_name">Billing Branch </label>
						<select class="form-control" name="branch" required="">  
							<option value=""> -- select -- </option>
						<?php
						 $sql2 = $conn_rrpl -> query("select username as branch from user where role='2' order by username asc");

						 while ($row2 = $sql2->fetch_assoc()) {
						 	$prtslt = "";
						 	// if($row['bill_branch']==$row2['branch']){
						 	// 	$prtslt = "selected";
						 	// }
						 	echo "<option value='".$row2['branch']."' ".$prtslt."> ".$row2['branch']." </option>";
						 }

						?>
						</select>
                     </div>
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">GST Number</label>
					  <input type="text" name="gst_no" id="gst_no" onchange="gst(this.value)" class="form-control">
					</div>
					

					<div class="form-group col-md-12">
                      <label for="consignor_name">Billing Address </label>
					  <input name="addr" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')"  class="form-control">
                    </div>

					<div class="form-group col-md-4">
                      <label for="consignor_name">Email Id</label>
					  <input type="email" name="email" class="form-control"  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&@]/,'')">
					</div>
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">Mobile Number</label>
					  <input type="text" name="mobile_no" class="form-control" oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')" >
					</div>
					
					 <div class="form-group col-md-4">
                      <label for="consignor_name">Payment Terms </label>
					  <input type="text" class="form-control" name="payment_terms"  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&]/,'')">
                    </div>
					
					 <div class="form-group col-md-4">
                      <label for="consignor_name">Rate Frequency  (In days) </label>
					  <input type="number" class="form-control" name="rate_frq" placeholder="" oninput="this.value=this.value.replace(/[^0-9]/,'')">
                    </div>
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">Next Rate Date</label>
					  <input type="date" name="rate_change_date" min="<?php echo date('Y-m-d'); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control">
                    </div>
					 
					
					<div class="form-group col-md-4">
                      <label for="consignor_name">EMD</label>
					  <input type="text" name="emd" class="form-control"  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&]/,'')">
					</div>

					
					<div class="form-group col-md-3">
                      <label for="consignor_name">In-Charge Name </label>
					  <input type="text" name="incharge" class="form-control" required  oninput="this.value=this.value.replace(/[^a-z A-Z 0-9,.&]/,'')">
                    </div>
					
					<div class="form-group col-md-3">
                      <label for="consignor_name">In-Charge Mobile </label>
					  <input type="text" name="incharge_mobile" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required>
                    </div>
					
					<div class="form-group col-md-3">
                      <label for="consignor_name">Contract Starts From </label>
					  <input type="date" name="con_start" max="<?php echo date('Y-m-d'); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control">
                    </div>
					
					<div class="form-group col-md-3">
                      <label for="consignor_name">Contract End Date </label>
					  <input type="date" name="con_end" min="<?php echo date('Y-m-d'); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control">
                    </div>
	   </div> 
	</div>
		<div class="modal-footer">
			<button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
			<input type="submit" id="" class="btn btn-primary" name="submit" value="SAVE" />
		</div>
	</form> 
 
<?php
mysqli_close($conn_rrpl);
?> 