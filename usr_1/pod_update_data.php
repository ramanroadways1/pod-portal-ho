<?php

  require('connect.php');
 
   $p = $conn_rrpl->real_escape_string($_REQUEST['p']);
   $f = $conn_rrpl->real_escape_string($_REQUEST['f']);
   $t = $conn_rrpl->real_escape_string($_REQUEST['t']);

$f = date("Y-m-d",$f);
$t = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
$statement = $connection->prepare("SELECT rcv_pod.*,  (SELECT name from consignor where id=consignor_id) as consignor, (SELECT name from billing_party where id=billing_party) as billparty, datediff(curdate(),exdate) as diff FROM rcv_pod where branch='$p' and (pod_date between '$f' and '$t') and billing!='1'");
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array(); 
 

 //  $btn= "<center> <div class='form-group' style='margin:0px !important;'> <input name='mark[]' type='checkbox' id='".$row["id"]."' value='".$row["id"]."'> <label for='".$row["id"]."'>   </label> </div>   </center> "; 
 //  $sub_array[] = $btn; 
	$sub_array[] = "<center>".$sno."</center>";
  $sub_array[] = $row["frno"]; 
  $sub_array[] = $row["branch"]; 
  $sub_array[] = $row["lrno"]; 
  $sub_array[] = $row["fm_date"]; 
  $sub_array[] = "<center>".$row["fm_amount"]."</center>"; 
  $sub_array[] = $row["pod_date"]; 
  
  if($row['self']=="1" || $row['nullify']=="1"){
  $sub_array[] = $row["exdate"]; 
  } else {
  $sub_array[] = "<input max='".date('Y-m-d')."' min='".$row["pod_date"]."' type='date' onchange='extend(this.value, ".$row["id"].")' value='".$row["exdate"]."' >"; 
  }

$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['pod_copy']) as $pod_copies)
{
  $copy_no++;
  $pod_files1[] = "<a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>Upload: $copy_no</a>";
 }
  $sub_array[] = implode(", ",$pod_files1);
    $sub_array[] = $row["diff"]." days"; 
  $sub_array[] = $row["consignor"]; 
  $sub_array[] = $row["billparty"]; 
  $sub_array[] = $row["billing_ofc"]; 

    $stat = "";
  if($row['self']=="1"){
    $stat = "SELF";
  }else if($row['nullify']=="1"){
    $stat = "FOC";
  } else {
    $stat = "<button onclick='nully(".$row["id"].")' class='btn btn-sm btn-warning'> <i class='fa fa-times' aria-hidden='true'></i>
 NUllify </button>";
  } 
  
  $sub_array[] = $stat; 
  
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 