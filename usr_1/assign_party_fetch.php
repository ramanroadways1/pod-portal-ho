<?php

  require('connect.php');
  
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
 
      $statement = $connection->prepare("SELECT * FROM consignor");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 

  $disgrp = "";
  $disgrp2 = "";
  if($row['group_id']!="0"){
    $disgrp = "disabled";
  } else {
    $disgrp2 = "disabled";
  }

  $sub_array[] = " <center> <button onclick='update(".$row['id'].")' class='btn btn-sm btn-primary' ".$disgrp."> <i class='fa fa-file-text-o '></i> <b> &nbsp; ASSIGN PARTY </b> </button> &nbsp; <button onclick='addon(".$row['id'].")' class='btn btn-sm btn-success' ".$disgrp2."> <i class='fa fa-file-text-o '></i> <b> &nbsp; ADDON PARTY </b> </button> </center> "; 
  $sub_array[] = $conn_rrpl -> real_escape_string($row['code']);
  $sub_array[] = "<span id='clr' style='cursor: pointer;' onclick='show(".$row['id'].")'>".$conn_rrpl -> real_escape_string($row['name'])."</span>";
  $sub_array[] = $conn_rrpl -> real_escape_string($row['gst']);

    $getgrp = "NA";
  if($row['group_id']=="0"){
    $getgrp = "NA";
  } else {
    $query = $conn_rrpl->query("Select * From billing_group where id='$row[group_id]'");
    $row2 = $query->fetch_assoc(); 
    $getgrp = $row2['name'];
  }

  $sub_array[] = "<center>".$getgrp."</center>";

  $billparty = $conn_rrpl -> real_escape_string($row['bill_party_id']); 
  $fields = array();

    if($billparty!='0' && $billparty!=''){
       $groupi = explode(",", $billparty); 
       $groupi_count = count($groupi);

        if($groupi_count > 1){
          for($i=0;$i<$groupi_count;$i++) { 
            $query = $conn_rrpl->query("Select * From billing_party where id='$groupi[$i]'");
            $row3 = $query->fetch_assoc(); 
            ${'prty_'.$i} = ucwords(strtolower($row3['name']));
            array_push($fields, ${'prty_'.$i});  
          }
          $getprty = implode(", ", $fields);
        } else { 
            $query = $conn_rrpl->query("Select * From billing_party where id='$billparty'");
            $row4 = $query->fetch_assoc(); 
            $getprty = ucwords(strtolower($row4['name']));
        } 
    } else {
      $getprty = "NA";
    }
 
  $sub_array[] = "<center>".$getprty."</center>";


  $addonparty = $conn_rrpl -> real_escape_string($row['addon_party']); 
  $fields = array();

    if($addonparty!='0' && $addonparty!=''){
       $groupi = explode(",", $addonparty); 
       $groupi_count = count($groupi);

        if($groupi_count > 1){
          for($i=0;$i<$groupi_count;$i++) { 
            $query = $conn_rrpl->query("Select * From billing_party where id='$groupi[$i]'");
            $row3 = $query->fetch_assoc(); 
            ${'prty_'.$i} = ucwords(strtolower($row3['name']));
            array_push($fields, ${'prty_'.$i});  
          }
          $adparty = implode(", ", $fields);
        } else { 
            $query = $conn_rrpl->query("Select * From billing_party where id='$addonparty'");
            $row4 = $query->fetch_assoc(); 
            $adparty = ucwords(strtolower($row4['name']));
        } 
    } else {
      $adparty = "NA";
    }

  $sub_array[] = "<center>".$adparty."</center>";

  $data[] = $sub_array;

} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>