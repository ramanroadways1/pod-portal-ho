<?php
 
	require('connect.php');

    $daterange = $conn_rrpl -> real_escape_string($_POST['daterange']);
    $seltype = $conn_rrpl -> real_escape_string($_POST['seltype']);
    // $Bilty_No = $conn_rrpl -> real_escape_string($_POST['bno']);
    // $branch = $conn_rrpl -> real_escape_string($_POST['branch']);

	function before ($thiss, $inthat)
	{
	    return substr($inthat, 0, strpos($inthat, $thiss));
	}

	function after ($thiss, $inthat)
	{
	    if (!is_bool(strpos($inthat, $thiss)))
	        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
	}
	 
	$fromdate = before('-', $daterange); 
	$fromdate = strtotime($fromdate); 
	$todate = after('-', $daterange); 
	$todate = strtotime($todate);
  
  	if($seltype=="report1"){

		$branch = $conn_rrpl -> real_escape_string($_POST['branch']); 

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>Vehicle_PlacedBy</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Broker_PAN</th>
					<th>Billing_Party</th>
					<th>Billing_Party_GST</th>
					<th>Billing_Party_PAN</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Branch</th>
					<th>Bill_No</th>
					<th>Bill_Amount</th>
					<th>C_GST</th>
					<th>S_GST</th>
					<th>I_GST</th>
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "market_bilty_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" } 
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
 
  	} else if($seltype=="report2"){
 
		$branch = $conn_rrpl -> real_escape_string($_POST['branch']); 

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>LR_Date</th>
					<th>Truck_No</th> 
					<th>Company</th> 
					<th>From</th>
					<th>To</th>
					<th>Broker</th>
					<th>Billing_Party</th> 
 					<th>Bilty_No</th>
					<th>Advance</th>
					<th>Type</th>
					<th>Branch</th>
					<th>Txn_Type</th>
					<th>Txn_Value</th>
					<th>Credit</th>
					<th>Debit</th>
					<th>Balance</th>
					<th>Narration</th> 
					<th>Timestamp</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "market_bilty_book.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
 

	} else if($seltype=="report3"){
 
		$branch = $conn_rrpl -> real_escape_string($_POST['branch']); 

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>LR_Date</th>
					<th>Truck_No</th> 
					<th>Company</th> 
					<th>From</th>
					<th>To</th>
					<th>Broker</th>
					<th>Billing_Party</th> 
 					<th>Bilty_No</th>
					<th>Txn_Id</th>
					<th>Expense</th>
					<th>Amount</th>
					<th>Narration</th>
					<th>Branch</th>
					<th>Date</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "market_bilty_expense.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
 

  	}
 