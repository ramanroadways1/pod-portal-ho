<?php

  require 'connect.php';

  $consignor_name = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['name']));
  $consignor_gst = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['gst']));
  $mobile = mysqli_real_escape_string($conn_rrpl, ($_POST['mobile']));
  $addr = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['address']));
  $pincode = mysqli_real_escape_string($conn_rrpl, strtoupper($_POST['pincode']));

   if(strlen($pincode)!=6)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Invaid Pincode $pincode !'
			})
			</script>"; 
			exit();
		} 
		
  $sql = mysqli_query($conn_rrpl, "SELECT `gst` FROM `consignor` WHERE `gst` = '$consignor_gst' AND gst!=''");
   if (mysqli_num_rows($sql) > 0)
   {

   	echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Duplicate GST No. $consignor_gst !'
		})
		</script>";	  
  	}  
   else
   { 
		$sql = mysqli_query($conn_rrpl, "SELECT `gst` FROM `consignor` WHERE `name` = '$consignor_name'");
		if(mysqli_num_rows($sql) > 0)
		{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Duplicate Name $consignor_name !'
			})
			</script>"; 
		} 
		else
		{
		
		$sql = mysqli_query($conn_rrpl, "INSERT INTO `consignor`(`name`, `gst`, mobile, addr,branch, pincode) 
			VALUES ('$consignor_name','$consignor_gst','$mobile', '$addr','SALES','$pincode')");
			 
		 $insert_id = mysqli_insert_id($conn_rrpl);
	
			$update = mysqli_query($conn_rrpl,"UPDATE consignor SET code='$insert_id' WHERE id='$insert_id'");
	
			 if ($sql) 
			  {
					echo "
					<script>
					Swal.fire({
					position: 'top-end',
					icon: 'success',
					title: 'New Consignor Updated.',
					showConfirmButton: false,
					timer: 1500
					})
					</script>"; 
			  }
			  else
			  { 
				echo "
				<script>
				Swal.fire({
				icon: 'error',
				title: 'Error !!!',
				text: '".mysqli_error($conn_rrpl)."'
				})
				</script>";  
			  }
		}

	}
?>