<?php
require_once 'connect.php';
$key=$_POST['key'];
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	

<style>
input[type='text'],input[type='number'],#ptob,#tdob,#chqid,#rtgsid,#langOpt{
border:1px solid #000;
}
.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
input[type="text"],input[type="number"]
{
	border:1px solid #000;
}
</style>


<?php
if($key=='LR')
{
$lrno1=strtoupper(mysqli_real_escape_string($conn_rrpl,$_POST['lrno']));

$chk1=mysqli_query($conn_rrpl,"SELECT frno,date,truck_no FROM freight_form_lr WHERE lrno='$lrno1'");
if(!$chk1)
{
	echo mysqli_error($conn_rrpl);
	exit();
}
if(mysqli_num_rows($chk1)==0)
{
	echo "<script>
	alert('No result found from LR No $lrno1');
	window.close();
	</script>";
	mysqli_close($conn_rrpl);
	exit();
}
else if(mysqli_num_rows($chk1)>1)
{
	echo "
<a href='./'><button class='btn btn-danger' style='margin-top:10px;margin-left:10px;'>Dashboard</button></a>	
	<div class='container'>
<br />	
<center><span style='color:red;font-family:Verdana'>There are follwing FreightMemo or Own-Truck Form from LR No $lrno1</span></center>	
<br />
	<table style='font-family:Verdana;font-size:12px;' class='table table-bordered'>
	<tr>
		<th>Srno</th>
		<th>FM No</th>
		<th>Truck No</th>
		<th>LR No</th>
		<th>LR Date</th>
	</tr>
	";
$no=1;	
	while($row1=mysqli_fetch_array($chk1))
	{
	echo "<tr>
			<td>$no</td>
			<td><a target='_blank' href='./show_fm.php?key=FM&idmemo=$row1[frno]'>$row1[frno]</td>
			<td>$row1[truck_no]</td>
			<td>$lrno1</td>
			<td>$row1[date]</td>
		</tr>";
$no++;	
	}
	echo "</table>";
	mysqli_close($conn_rrpl);
	exit();
}
else
{
	$row1=mysqli_fetch_array($chk1);
	$idmemo=$row1['frno'];
	
		if(substr($idmemo,3,1)!="F")
		{
			echo "<br />
			<center>
			<h3 style='font-family:Verdana'>This is Own Truck LR - Form : <font color='blue'>$idmemo</font></h3>
			<form action='view_own.php' method='post' target='_blank'>
			<input type='hidden' name='memo' value='$idmemo' />
			<input type='submit' name='submit' style='font-family:Verdana' class='btn btn-danger' value='View here !' />
			</form>
			</center>
			";
			mysqli_close($conn_rrpl);
			exit();
		}
}
}
else if($key=='FM')
{
$idmemo=strtoupper($_REQUEST['idmemo']);

if(substr($idmemo,3,1)!="F")
{
echo "<br >
	<br />
	<center>
			<h3 style='font-family:Verdana'>This is Own Truck LR - Form : <font color='blue'>$idmemo</font></h3>
			<form action='view_own.php' method='post' target='_blank'>
			<input type='hidden' name='memo' value='$idmemo' />
			<input type='submit' name='submit' style='font-family:Verdana' class='btn btn-danger' value='View here !' />
			</form>
			</center>
	";
	mysqli_close($conn_rrpl);
	exit();
}
}
else
{
	echo "Invalid Entry..";
	mysqli_close($conn_rrpl);
	exit();
}

$res=mysqli_query($conn_rrpl,"SELECT freight_form.newdate,freight_form.truck_no,freight_form.branch,freight_form.company,freight_form.actualf,
freight_form.newtds,freight_form.dsl_inc,freight_form.newother,freight_form.tds,freight_form.totalf,freight_form.totaladv,freight_form.ptob,
freight_form.adv_date,freight_form.cashadv,freight_form.chqadv,freight_form.chqno,freight_form.disadv,freight_form.rtgsneftamt,freight_form.narre,
freight_form.baladv,freight_form.paidto,freight_form.sign_cash,freight_form.sign_adv,freight_form.update_lr,mk_broker.name as b_name,
mk_broker.mo1 as b_mobile,mk_broker.pan as b_pan,mk_driver.name as d_name,mk_driver.mo1 as d_mobile,mk_driver.pan as d_lic,mk_truck.name as o_name,
mk_truck.mo1 as o_mobile,mk_truck.pan as o_pan FROM 
freight_form,mk_broker,mk_driver,mk_truck WHERE freight_form.frno='$idmemo' AND freight_form.bid=mk_broker.id AND freight_form.did=mk_driver.id AND freight_form.oid=mk_truck.id");
if(!$res)
{
	echo mysqli_error($conn_rrpl);
	mysqli_close($conn_rrpl);
	exit();
}
$ro=mysqli_fetch_array($res);
$adv_to1=$ro['ptob'];
$bal_to1=$ro['paidto'];
$fmdt=$ro['newdate'];
$adv_dt=$ro['adv_date'];
$comp1=$ro['company'];
$update_lr=$ro['update_lr'];
$adv_sign=$ro['sign_adv'];
$cash_sign=$ro['sign_cash'];

$data_table='<table class="table table-bordered" style="margin-top:5px;font-family:Verdana;font-size:11px">

	<tr>   
                        <th>Act Frt</th>
						<th>Load</th>
						<th>Dsl Inc</th>
						<th>Other</th>
						<th>TDS</th>
						<th>Total Frt</th>
						<th>Adv to</th>
						<th>Cash</th>
						<th>Cheq</th>
						<th>Diesel</th>
						<th>RTGS/NEFT</th>
						<th>Total Adv</th>
						<th>Balance</th>
                        <th>Narr.</th>
</tr>
<tr>
				<td>'.$ro['actualf'].'</td>
				<td>'.$ro['newtds'].'</td>
				<td>'.$ro['dsl_inc'].'</td>
				<td>'.$ro['newother'].'</td>
				<td>'.$ro['tds'].'</td>
				<td>'.$ro['totalf'].'</td>
				<td>'.$ro['ptob'].'</td>
				<td>'.$ro['cashadv'].'</td>
				<td>'.$ro['chqadv'].'</td>
				<td>'.$ro['disadv'].'</td>
				<td>'.$ro['rtgsneftamt'].'</td>
				<td>'.$ro['totaladv'].'</td>
				<td>'.$ro['baladv'].'</td>
                <td>'.$ro['narre'].'</td>
	</tr>
</table>	
';

if($adv_sign=='')
{	
echo "
	<input type='hidden' id='sign_box' value='0'>
	";
}
else
{
	echo "
	<input type='hidden' id='sign_box' value='1'>
	";
}

$fmdate=date('d-M-y', strtotime($fmdt));
$adv_date=date('d-M-y', strtotime($adv_dt));

if($adv_to1=='')
{
	echo "<script>
	alert('Advance of this Freight Memo is pending yet.');
	window.close();
	</script>";
	mysqli_close($conn_rrpl);
	exit();
}
else if($bal_to1=='')
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
<center>
	<img style="margin-top:150px" src="../../b5aY6EZzK52NA8F/load.gif" />
</center>
</div>

</head>

<body style="overflow-x: auto !important;font-family:Verdana;" onload="hideall();">

<style type="text/css" media="print">
/*  @media print {

  body {
   zoom:80%;
 }
}
*/
</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.container-fluid * { visibility: visible}
.container-fluid { position: absolute; top: 0; left: 0; }
}
</style>

<div id="result_main"></div>

<button type="button" onclick="window.close();" style="margin-left:10px;margin-top:10px;letter-spacing:1px;font-weight:bold" class="btn btn-primary">Close</button>

<div class="container-fluid">
<div class="row" style="font-family:Verdana">
	 <center>
	 <div>
	<span class="" style="font-size:15px;letter-spacing:1px">Freight Memo : <?php echo strtoupper($idmemo); ?></span> 
	</div>
	<div style="font-weight:bold" class="pull-right">
	<span style="font-size:12px;margin-right:10px">FM Date : <?php echo $fmdate; ?></span>
		<?php
		if($adv_to1!='')
		{			
		?>		
			<span style="font-size:12px;margin-right:5px">Adv Date : <?php echo $adv_date; ?></span>
		<?php 
		}
		else
		{
			echo "<style>#data_table_div{display:none}</style>";
		}
		?>
	</div>
	</center>
</div>
<input type="hidden" id="fids" name="fids" value="<?php echo $idmemo; ?>" />
		<br />
<div class="row">

<div class="col-md-12 table-responsive" style="letter-spacing:1px; font-size:10px; font-family: 'Verdana', cursive;overflow-x:auto">
<table border="0" style="width:100%;font-size:11px">
<tr>
	<td>
		<label>Truck No:</label>
		<?php echo $ro['truck_no'];?>
	</td>  

	<td>
		<label>Company:</label>
		<?php echo $ro['company']; ?>
	</td>

	<td align="right">
		<label>Branch:</label>
		<?php echo $ro['branch']; ?>
	</td>
</tr>	

<tr>
		<td>
			<label>Broker:</label>
			<?php echo $ro['b_name'];
			?>
		</td>
		
		<td>
			<label>Broker Mo:</label>
			<?php echo $ro['b_mobile'];
			?>
		</td>

		<td align="right">
			<label>Broker PAN:</label>
			<?php echo $ro['b_pan']; 
			?>
		</td>
	</tr>
 
 <tr>      
		<td>
			<label>Driver:</label>
			<?php echo $ro['d_name']; ?>
		</td>
		
		<td>
			<label>Driver Mo:</label>
			<?php echo $ro['d_mobile']; ?>
		</td>

		<td align="right">
			<label>Driver LIC.:</label>
			<?php echo $ro['d_lic']; ?>
		</td>
	</tr>		

	<tr>
		<td>
			<label>Owner:</label>
			<?php echo $ro['o_name']; 
			?>
		</td>

		<td>
			<label>Owner Mo:</label>
			<?php echo $ro['o_mobile'];
			?>
		</td>
		
		<td align="right">
			<label>Owner PAN:</label>
			<?php echo $ro['o_pan']; 
			?>
		</td>
	</tr>	

</table>
</div>
</div>

<br />

<!-- READ FM ADVANCE TABLE -->
<div class="row" id="data_table_div">
	<div class="col-md-12" style="font-family: 'Verdana', cursive; color:#000; font-size:11px;">
	<span style="color:blue;font-size:13px;letter-spacing:1px;font-weight:bold">Advance Details -</span>
		<div style="overflow-x:auto" class="records_content2 table-responsive"><?php echo $data_table; ?></div>
	</div>
</div>

<!-- LOAD LR DATA ---->
<script type="text/javascript">
lrFetch();
function lrFetch()
{ 
	var sign22=$("#sign_box").val();
	if(sign22==0)
	{
		$("#sign_result").html("Plz upload signature..");
		$("#f_btn").attr("disabled",true);
	}
	else
	{
		$("#sign_result").html("");
		$("#f_btn").attr("disabled",false);
	}
	$("#new").show();
	jQuery.ajax({
	url: "./readRecords.php",
	data: 'fids=' + $("#fids").val(),
	type: "POST",
	success: function(data) {
	$(".records_content").html(data);
	$("#new").hide();
	},
	error: function() {}
	});
}					
</script>					                               

<div class="row">
	<div class="col-md-12">
<h4 style="font-family: 'Verdana', cursive;color:blue;font-size:13px;font-weight:bold">LR Details - </h4>
	<div style="overflow-x:auto" class="records_content table-responsive"></div>
</div>
</div>

<!-- ENDSS LOAD LR DATA ---->
<?php
if($adv_sign!='' AND $cash_sign!='')
{
?>
<table border="0" style="width:100%">
	<tr>
		<td align="center"><label style="font-size:13px;">Cashier Sign</label></td>
		<td align="center"><label style="font-size:13px;">Adv Rcvr</label></td>
	</tr>	
	<tr>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../../b5aY6EZzK52NA8F/<?php echo $cash_sign; ?>"></td>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../../b5aY6EZzK52NA8F/<?php echo $adv_sign; ?>"></td>
	</tr>	
</table>
<?php
}
?> 

</div>

<div class="col-md-12" style="font-family: 'Verdana', cursive; color:#000; font-size:30px; font-weight:bold; text-align:center;">
<center>
<button type="button" id="btnprint" onclick="print();" class="btn btn-danger btn-lg">Print Freight MEMO</button>
</center>
</div>
</div>
</body>
</html>
<?php
}
else
{
?>
<style>
body{
background-color:#fff; color:#000;}
</style>
<br>
<center>
<h1 style=" font-family: 'Verdana', cursive; font-size:27px; letter-spacing:2px;">Balance Paid & LR has been Closed !
</h1>
<?php 
echo "<h3><a class='ji' style='text-transform:uppercase;font-family:Verdana' href='full_fm.php?idmemo=$idmemo'>Click Here : $idmemo </a></h3>";
?>
<br />
<button type="button" onclick="window.close();" class="btn btn-danger" style="font-family:Verdana">Close</button></a>
</center>
</html>
<?php
}
?>