<?php

  require('connect.php');
 
   $p = $conn_rrpl->real_escape_string($_REQUEST['p']);
   $f = $conn_rrpl->real_escape_string($_REQUEST['f']);
   $t = $conn_rrpl->real_escape_string($_REQUEST['t']);

$f = date("Y-m-d",$f);
$t = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );

if($p!="ALL"){
$statement = $connection->prepare("SELECT mkt_bilty.company, mkt_bilty.lrdate, mkt_bilty.broker, mkt_bilty.billing_party, mkt_bilty.tno,mkt_bilty.frmstn,mkt_bilty.tostn,mkt_bilty.bilty_no, dairy.bilty_book.trans_id, dairy.bilty_book.type, dairy.bilty_book.branch, dairy.bilty_book.date, dairy.bilty_book.trans_type, dairy.bilty_book.trans_value, dairy.bilty_book.credit, dairy.bilty_book.debit, dairy.bilty_book.balance, dairy.bilty_book.narration, dairy.bilty_book.timestamp FROM mkt_bilty RIGHT JOIN dairy.bilty_book ON mkt_bilty.bilty_no = dairy.bilty_book.bilty_no WHERE (mkt_bilty.lrdate BETWEEN '$f' AND '$t' AND mkt_bilty.branch='$p')");
} else {
  $statement = $connection->prepare("SELECT mkt_bilty.company, mkt_bilty.lrdate, mkt_bilty.broker, mkt_bilty.billing_party, mkt_bilty.tno,mkt_bilty.frmstn,mkt_bilty.tostn,mkt_bilty.bilty_no, dairy.bilty_book.trans_id, dairy.bilty_book.type, dairy.bilty_book.branch, dairy.bilty_book.date, dairy.bilty_book.trans_type, dairy.bilty_book.trans_value, dairy.bilty_book.credit, dairy.bilty_book.debit, dairy.bilty_book.balance, dairy.bilty_book.narration, dairy.bilty_book.timestamp FROM mkt_bilty RIGHT JOIN dairy.bilty_book ON mkt_bilty.bilty_no = dairy.bilty_book.bilty_no WHERE (mkt_bilty.lrdate BETWEEN '$f' AND '$t')");
}

$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array();  
            $sub_array[] = $row["lrdate"];
            $sub_array[] = $row["tno"];
            $sub_array[] = $row["company"];
            $sub_array[] = $row["frmstn"];
            $sub_array[] = $row["tostn"];
            $sub_array[] = $row["broker"];
            $sub_array[] = $row["billing_party"];
            $sub_array[] = $row["bilty_no"];
            $sub_array[] = $row["trans_id"];
            $sub_array[] = $row["type"];
            $sub_array[] = $row["branch"];
            $sub_array[] = $row["trans_type"];
            $sub_array[] = $row["trans_value"];
            $sub_array[] = $row["credit"];
            $sub_array[] = $row["debit"];
            $sub_array[] = $row["balance"];
            $sub_array[] = $row["narration"];
            $sub_array[] = $row["timestamp"]; 
    
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 