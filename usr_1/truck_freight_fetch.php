<?php
 
	require('connect.php');

    $daterange = $conn_rrpl -> real_escape_string($_POST['daterange']);
    // $branch = $conn_rrpl -> real_escape_string($_POST['branch']);

	function before ($thiss, $inthat)
	{
	    return substr($inthat, 0, strpos($inthat, $thiss));
	}

	function after ($thiss, $inthat)
	{
	    if (!is_bool(strpos($inthat, $thiss)))
	        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
	}
	 
	$fromdate = before('-', $daterange); 
	$fromdate = strtotime($fromdate); 
	$todate = after('-', $daterange); 
	$todate = strtotime($todate);
  
 
    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
						 <th>Cash/Cheque</th>  
                         <th>Truck_No</th>  
                         <th>Credit_Date</th>  
                         <th>LR_Date</th>  
                         <th>From</th>  
						 <th>To</th>  
                         <th>Company</th>  
						 <th>Market_Bilty_No</th>  
                         <th>Amount</th>  
                         <th>Cheque_No</th>  
                         <th>Branch</th>  
                         <th>Narration</th>  
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "truck_freight_data.php?f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
"dom": "lBfrtip",
"ordering": true,
"buttons": [
"copy", "csv", "excel", "print"
],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" } 
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
 