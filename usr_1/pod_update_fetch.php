<?php

// Array ( [seltype] => branchi [daterange] => 01/01/2020 - 01/22/2020 [fromdate] => 2020-01-01 [todate] => 2020-01-22 [branch] => ABUROAD [memoid] => )
 
	require('connect.php');
 	error_reporting(0);
    $seltype = $conn_rrpl -> real_escape_string($_POST['seltype']);
    $daterange = $conn_rrpl -> real_escape_string($_POST['daterange']);
    $branch = $conn_rrpl -> real_escape_string($_POST['branch']);
    $lrno = $conn_rrpl -> real_escape_string($_POST['lrno']); 

function before ($thiss, $inthat)
{
    return substr($inthat, 0, strpos($inthat, $thiss));
}

function after ($thiss, $inthat)
{
    if (!is_bool(strpos($inthat, $thiss)))
        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
}
 
$fromdate = before('-', $daterange); 
$fromdate = strtotime($fromdate); 
$todate = after('-', $daterange); 
$todate = strtotime($todate);


    if($seltype=="brancho"){

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=""> Sno </th> 
				<th style=""> Vou No </th> 
				<th style=""> Branch </th> 
				<th style=""> LR No </th> 
		        <th style=""> FM Date </th>
		        <th style=""> Amount </th> 
		        <th style=""> POD Date </th> 
		        <th style=""> Extended </th> 
		        <th style=""> Upload </th> 
		        <th style=""> Pending </th> 
		        <th style=""> Consignor </th> 
		        <th style=""> Billing Party </th> 
		        <th style=""> Billing Branch </th> 
		        <th style=""> Status </th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "pod_update_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" } 
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); 

		';

		echo 'function nully(val){  
		const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
		confirmButton: "btn btn-success",
		cancelButton: "btn btn-danger"
		},
		buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
		title: "Are you sure ?",
		text: "You won\'t be able to revert this !",
		icon: "warning",
		showCancelButton: true,
		confirmButtonText: "Yes, Nullify it !",
		cancelButtonText: "No, Cancel !",
		reverseButtons: true
		}).then((result) => {
		if (result.value) {
		  
		  $("#loadicon").show();  
		  $.ajax({  
		        url:"pod_update_null.php",  
		        method:"post",  
		        data:{val:val},  
		        success:function(data){  
		             $("#response").html(data);  
		             $("#loadicon").hide();  
					 $("#user_data").DataTable().ajax.reload();   
		        }  
		   });  
		}  
		}) 
		} 
  
		function extend(val1,val2){
		$("#loadicon").show(); 
		var val1 = val1;  
		var val2 = val2;  
		$.ajax({  
		    url:"pod_update_save.php",  
		    method:"post",  
		    data:{val1:val1,val2:val2},  
		    success:function(data){  
		         $("#response").html(data);  
		         $("#loadicon").hide(); 
				 $("#user_data").DataTable().ajax.reload();   
		    }  
		});
		} </script>';

    } 