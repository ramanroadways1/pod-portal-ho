<?php

  require('connect.php');
  
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
 
      $statement = $connection->prepare("SELECT * FROM consignee order by id asc");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 

  $hide = $conn_rrpl -> real_escape_string($row['hide']);
  if($hide=="0"){
    $hide = "NO";
    $color = "";
  } else {
    $hide = "YES";
    $color = "<font color='red'>";
  }

  $sub_array[] = " <center> <button onclick='update(".$row['id'].")' class='btn btn-sm btn-warning'> <i class='fa fa-edit '></i> <b>EDIT</b> </button>  &nbsp;  <button onclick='merge(".$row['id'].")' class='btn btn-sm btn-primary'> <i class='fa fa-filter '></i> <b>MERGE</b> </button> </center>"; 
  $sub_array[] = $color.preg_replace("/[^0-9a-zA-Z ]/", "", $row['code']);
  $sub_array[] = $color.preg_replace("/[^0-9a-zA-Z ]/", "", $row['name']);
  $sub_array[] = $color.preg_replace("/[^0-9a-zA-Z ]/", "", $row['gst']); 
  $sub_array[] = $color.preg_replace("/[^0-9a-zA-Z ]/", "", $row['mobile']); 
  $sub_array[] = $color.preg_replace("/[^0-9a-zA-Z ]/", "", $row['addr']);
  $sub_array[] = $color.preg_replace("/[^0-9a-zA-Z ]/", "", $row['pincode']);
  $sub_array[] = $color.preg_replace("/[^0-9a-zA-Z ]/", "", $row['branch']); 
  
  $sub_array[] = $color.$hide;
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['timestamp']); 

  $data[] = $sub_array;

} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>