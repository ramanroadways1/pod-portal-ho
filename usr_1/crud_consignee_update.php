<?php
	require('connect.php'); 
	$id =  $conn_rrpl -> real_escape_string($_POST['id']);
  
    $members = $conn_rrpl->query("SELECT * FROM `consignee` where id=$id");
    $mem = mysqli_fetch_assoc($members); 
?>

<script type="text/javascript">
function gstf(gst){
    var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
    
    if (gstinformat.test(gst)) {
     return true;
    } else {
 		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Please Enter Valid GSTIN Number !'
		}) 
		$("#gst").val('');
        $("#gst").focus();
    }
    
}
</script>	
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}
</style>
<form method="post" action="" id="updatereq" role="form" autocomplete="off">
	<div class="modal-body">
<p style="color: #444;"> UPDATE CONSIGNEE  <button type="button" class="close" data-dismiss="modal"> &times; </button> <p style="border-bottom: 1px solid #ccc;"></p>
		</p>

		
		<div class="row">
 		<div class="form-group col-md-2">
			<label for="phone">CODE</label>
				<input type="text" class="form-control" id="" name="code" value="<?php echo $mem['code'];?>" readonly />
				<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')"  type="hidden" name="id" value="<?php echo $mem['id'];?>" readonly />
				<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')"  type="hidden" name="check_consignee" value="<?php echo $mem['name'];?>" readonly />
				 
		</div>  
		<div class="form-group col-md-10">
			<label for="phone"> NAME <font color="red"><sup>*</sup></font></label>
				<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')"  type="text" class="form-control" id="" name="name" value="<?php echo $mem['name'];?>" required/>
		</div>
		<div class="form-group col-md-6">
			<label for="phone"> GST  </label>
				<input onchange="gstf(this.value)"  oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')"  type="text" class="form-control" id="gst" name="gst" value="<?php echo $mem['gst'];?>" />
		</div>
		<div class="form-group col-md-6">
			<label for="phone"> MOBILE  </label>
				<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')"  type="text" class="form-control" id="" name="mobile" value="<?php echo $mem['mobile'];?>" />
		</div>
		
		<div class="form-group col-md-6">
			<label for="phone"> PIN CODE <font color="red"><sup>*</sup></font></label>
				<input oninput="this.value=this.value.replace(/[^0-9]/,'')" required  type="text" class="form-control" id="" name="pincode" value="<?php echo $mem['pincode'];?>" />
		</div>
		<div class="form-group col-md-6">
			<label for="phone"> BRANCH </label>
				<input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')"  type="text" class="form-control" id="" name="" value="<?php echo $mem['branch'];?>" readonly />
		</div>
		
		<div class="form-group col-md-9">
			<label for="phone"> ADDRESS <font color="red"><sup>*</sup></font></label>
				<input type=""  required oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.()&-]/,'')"   type="text" class="form-control" id="" name="address" value="<?php echo $mem['addr'];?>"> 
		</div>
		
		<div class="form-group col-md-3">
			<label for="phone"> HIDE </label>
			<select name="hide" class="form-control">
				<option value="">  -- select -- </option>
				<option value="1" <?php if($mem['hide']=="1"){ echo "selected"; } ?>> YES </option>
				<option value="0" <?php if($mem['hide']=="0"){ echo "selected"; } ?>> NO </option>
			</select>
		</div> 
	   </div> 
	</div>
		<div class="modal-footer">
			<button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
			<input type="submit" id="updatereqbtn" class="btn btn-primary" name="submit" value="UPDATE" />
		</div>
	</form> 
 
<?php
mysqli_close($conn_rrpl);
?> 