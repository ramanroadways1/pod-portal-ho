<?php

  require('connect.php');
  
  $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
 
      $statement = $connection->prepare("SELECT * FROM consignor where approval='0'");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 

  $hide = $conn_rrpl -> real_escape_string($row['hide']);
  if($hide=="0"){
    $hide = "NO";
    $color = "";
  } else {
    $hide = "YES";
    $color = "<font color='red'>";
  }

  $sub_array[] = " <center> <button onclick='update(".$row['id'].")' class='btn btn-sm btn-warning'> <i class='fa fa-edit '></i> <b> UPDATE </b> </button>  &nbsp;  <button onclick='approve(".$row['id'].")' class='btn btn-sm btn-success'> <i class='fa fa-check'></i> <b>APPROVE</b> </button> </center>"; 
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['code']);
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['name']);
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['gst']);
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['mobile']);
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['addr']);
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['pincode']);
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['branch']); 
  $sub_array[] = $color.$hide;
  $sub_array[] = $color.$conn_rrpl -> real_escape_string($row['timestamp']); 

  $data[] = $sub_array;

} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>