<?php

	require('connect.php');

	error_reporting(0);

    $seltype = $conn_rrpl -> real_escape_string($_POST['seltype']);
    $branch = $conn_rrpl -> real_escape_string($_POST['branch']);
    $name = $conn_rrpl -> real_escape_string($_POST['name']);
    $groupname = $conn_rrpl -> real_escape_string($_POST['groupname']);
    $groupid = $conn_rrpl -> real_escape_string($_POST['groupid']);
    $conid = $conn_rrpl -> real_escape_string($_POST['conid']);

    if($seltype=="branch"){
	 
		echo '
		<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
				<th style=" text-align: center; font-size: 13px; color:#444;"> Code </th> 
				<th style=" text-align: center; font-size: 13px; color:#444;"> Group </th> 
		        <th style=" text-align: center; font-size: 13px; color:#444;"> Consignor Name </th>
		        <th style=" text-align: center; font-size: 13px; color:#444;"> GST No. </th> 
				
				<th style="width: 25%;">
				<button onclick="SelectAll()" class="btn btn-warning btn-sm" style="border: 1px solid #000; color: #fff; padding: 4px 6px; font-size: 11px; margin-top: 4px;"> Select All </button> &nbsp; &nbsp; 
				<button onclick="unSelectAll()" class="btn btn-warning btn-sm" style="border: 1px solid #000; color: #fff; padding: 4px 6px; font-size: 11px; margin-top: 4px;"> Unselect All </button></th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "showhide_fetch_list.php?p='.$branch.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		"targets":[4],
		"orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
   
		function action(val,stat){
		$("#loadicon").show();
		$.ajax({
		type: "POST",
		url: "showhide_action.php?p='.$branch.'",
		data: {id: val, act: stat},
		success: function(data){ 
		// alert(data);
		$("#user_data").DataTable().ajax.reload();
		$("#loadicon").hide(); 
		}
		}); 
		}
 
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} );

	function SelectAll()
	{ 
		Swal.fire({
		title: "Are you sure ?",
		icon: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes, Select All !"
		}).then((result) => {
		  if (result.value) {
			$("#loadicon").show();
			$.ajax({
			type: "POST",
			url: "showhide_select_all.php?p='.$branch.'",
			data: "",
			success: function(data){
			$("#response").html(data);  
			$("#loadicon").hide();
			$("#user_data").DataTable().ajax.reload();
			},
			error: function(){
			alert("Error: Something went Wrong !");
			}
			});
		  }
		})
	}

	function unSelectAll()
	{
		Swal.fire({
		title: "Are you sure ?",
		icon: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes, Unselect All !"
		}).then((result) => {
		  if (result.value) {
			$("#loadicon").show();
			$.ajax({
			type: "POST",
			url: "showhide_unselect_all.php?p='.$branch.'",
			data: "",
			success: function(data){
			$("#response").html(data);  
			$("#loadicon").hide();
			$("#user_data").DataTable().ajax.reload();
			},
			error: function(){
			alert("Error: Something went Wrong !");
			}
			});
		  }
		})  
	}
		</script>';
  
    } else if($seltype=="consignor"){
 
		echo '
		<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
		        <th style=" text-align: center; font-size: 13px; color:#444;"> Consignor Name </th>
		        <th style=" text-align: center; font-size: 13px; color:#444;"> Branch </th> 
				
				<th style="width: 25%;">
				<button onclick="SelectAll()" class="btn btn-warning btn-sm" style="border: 1px solid #000; color: #fff; padding: 4px 6px; font-size: 11px; margin-top: 4px;"> Select All </button> &nbsp; &nbsp; 
				<button onclick="unSelectAll()" class="btn btn-warning btn-sm" style="border: 1px solid #000; color: #fff; padding: 4px 6px; font-size: 11px; margin-top: 4px;"> Unselect All </button></th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "showhide_branch_fetch_list.php?p='.$conid.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		"targets":[2],
		"orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
   
		function action(val,stat){
		$("#loadicon").show();
		$.ajax({
		type: "POST",
		url: "showhide_branch_action.php?p='.$conid.'",
		data: {id: val, act: stat},
		success: function(data){ 
		// alert(data);
		$("#user_data").DataTable().ajax.reload();
		$("#loadicon").hide(); 
		}
		}); 
		}
 
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} );

	function SelectAll()
	{ 
		Swal.fire({
		title: "Are you sure ?",
		icon: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes, Select All !"
		}).then((result) => {
		  if (result.value) {
			$("#loadicon").show();
			$.ajax({
			type: "POST",
			url: "showhide_branch_select_all.php?p='.$conid.'",
			data: "",
			success: function(data){
			$("#response").html(data);  
			$("#loadicon").hide();
			$("#user_data").DataTable().ajax.reload();
			},
			error: function(){
			alert("Error: Something went Wrong !");
			}
			});
		  }
		})
	}

	function unSelectAll()
	{
		Swal.fire({
		title: "Are you sure ?",
		icon: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes, Unselect All !"
		}).then((result) => {
		  if (result.value) {
			$("#loadicon").show();
			$.ajax({
			type: "POST",
			url: "showhide_branch_unselect_all.php?p='.$conid.'",
			data: "",
			success: function(data){
			$("#response").html(data);  
			$("#loadicon").hide();
			$("#user_data").DataTable().ajax.reload();
			},
			error: function(){
			alert("Error: Something went Wrong !");
			}
			});
		  }
		})  
	}
		</script>';	

    } else if($seltype=="group"){

    	echo '<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
		        <th style=" text-align: center; font-size: 13px; color:#444;"> Consignor Name </th>
		        <th style=" text-align: center; font-size: 13px; color:#444;"> Group </th> 
				
				<th style="width: 25%; Text-align: center; font-size: 13px; color:#444;"> Update </th> 
 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "showhide_group_fetch_list.php?p='.$groupid.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		"targets":[2],
		"orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
   
		function action(val,stat){
		$("#loadicon").show();
		$.ajax({
		type: "POST",
		url: "showhide_group_action.php?p='.$groupid.'",
		data: {id: val, act: stat},
		success: function(data){ 
		$("#response").html(data);  
		$("#user_data").DataTable().ajax.reload();
		$("#loadicon").hide(); 
		}
		}); 
		}
 
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); 
		</script>';
    }
