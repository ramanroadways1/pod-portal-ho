<?php

  require('connect.php');
 
   $p = $conn_rrpl->real_escape_string($_REQUEST['p']);
   $f = $conn_rrpl->real_escape_string($_REQUEST['f']);
   $t = $conn_rrpl->real_escape_string($_REQUEST['t']);

$f = date("Y-m-d",$f);
$t = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );

if($p!="ALL"){
$statement = $connection->prepare("SELECT (SELECT cgst_amt from bill_gst where bill_gst.bill_no=mkt_bilty.bill_no order by id desc limit 1) as cgst , (SELECT sgst_amt from bill_gst where bill_gst.bill_no=mkt_bilty.bill_no order by id desc limit 1) as sgst , (SELECT igst_amt from bill_gst where bill_gst.bill_no=mkt_bilty.bill_no order by id desc limit 1) as igst , bill_no, bill_amount, weight, bill_datetime, mkt_bilty.date,lrdate,bilty_no,lr_by,billing_type,veh_placer,plr,broker,billing_party,tno, frmstn,tostn,awt,cwt,rate,tamt,mkt_bilty.branch,billing_party.gst as bill_gst,billing_party.pan as bill_pan, broker.pan as broker_pan FROM mkt_bilty,dairy.billing_party,dairy.broker WHERE lrdate BETWEEN '$f' AND '$t' AND mkt_bilty.branch='$p' and billing_party=billing_party.name AND broker=broker.name ORDER BY mkt_bilty.id ASC");
} else {
  $statement = $connection->prepare("SELECT (SELECT cgst_amt from bill_gst where bill_gst.bill_no=mkt_bilty.bill_no order by id desc limit 1) as cgst , (SELECT sgst_amt from bill_gst where bill_gst.bill_no=mkt_bilty.bill_no order by id desc limit 1) as sgst , (SELECT igst_amt from bill_gst where bill_gst.bill_no=mkt_bilty.bill_no order by id desc limit 1) as igst , bill_no, bill_amount, weight, bill_datetime, mkt_bilty.date,lrdate,bilty_no,lr_by,billing_type,veh_placer,plr,broker,billing_party,tno, frmstn,tostn,awt,cwt,rate,tamt,mkt_bilty.branch,billing_party.gst as bill_gst,billing_party.pan as bill_pan, broker.pan as broker_pan FROM mkt_bilty,dairy.billing_party,dairy.broker WHERE lrdate BETWEEN '$f' AND '$t' AND billing_party=billing_party.name AND broker=broker.name ORDER BY mkt_bilty.id ASC");
}

$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array();  
            $sub_array[] = $row["date"];
            $sub_array[] = $row["lrdate"];
            $sub_array[] = $row["bilty_no"];
            $sub_array[] = $row["billing_type"];
            $sub_array[] = $row["veh_placer"];
            $sub_array[] = $row["plr"];
            $sub_array[] = $row["broker"];
            $sub_array[] = $row["broker_pan"];
            $sub_array[] = $row["billing_party"];
            $sub_array[] = $row["bill_gst"];
            $sub_array[] = $row["bill_pan"];
            $sub_array[] = $row["tno"];
            $sub_array[] = $row["frmstn"];
            $sub_array[] = $row["tostn"];
            $sub_array[] = $row["awt"];
            $sub_array[] = $row["cwt"];
            $sub_array[] = $row["rate"];
            $sub_array[] = $row["tamt"];
            $sub_array[] = $row["branch"];
            $sub_array[] = $row["bill_no"];
            $sub_array[] = $row["bill_amount"];
            $sub_array[] = $row["cgst"];
            $sub_array[] = $row["sgst"];
            $sub_array[] = $row["igst"];
            $sub_array[] = $row["weight"];
            $sub_array[] = $row["bill_datetime"];
    
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 