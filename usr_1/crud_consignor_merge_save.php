<?php

require 'connect.php';
 
	$beforename = $conn_rrpl -> real_escape_string(strtoupper($_POST['beforename'])); 
	$aftername = $conn_rrpl -> real_escape_string(strtoupper($_POST['aftername'])); 
	$beforeid = $conn_rrpl -> real_escape_string($_POST['beforeid']); 
	$afterid = $conn_rrpl -> real_escape_string($_POST['afterid']);  
 
	$con1_id_old = mysqli_query($conn_rrpl,"SELECT id FROM consignor WHERE id='$beforeid'");
	$con1_id_new = mysqli_query($conn_rrpl,"SELECT id, gst FROM consignor WHERE id='$afterid'"); 
	$row_old = mysqli_fetch_array($con1_id_old);
	$row_new = mysqli_fetch_array($con1_id_new);
  
	$old_id = $row_old['id'];
	$new_id = $row_new['id'];
	$new_gst = $row_new['gst'];
  
	if($old_id=='' || $new_id=='') 
	{
		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Unable to Identify Consignor !'
		})
		</script>";	  

	} else {

		if($old_id!=$new_id)
 		{ 
 				try {
				$conn_rrpl->query("START TRANSACTION"); 

				$sql = "UPDATE consignor SET hide='1' WHERE id='$old_id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "update lr_sample set consignor='$aftername',con1_id='$new_id',con1_gst='$new_gst' where con1_id='$old_id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "update lr_sample set consignor='$aftername',con1_id='$new_id',con1_gst='$new_gst' where con1_id='' 
				AND consignor='$beforename'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}  

				$sql = "update freight_form_lr set consignor='$aftername', con1_id='$new_id' where con1_id='$old_id'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$sql = "update freight_form_lr set consignor='$aftername',con1_id='$new_id' where con1_id='' AND 
				consignor='$beforename'";	 
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				}   

				$content = "Consignor Merged: ".$beforename." to ".$aftername;
				$sql = "INSERT INTO `billing_log` (`content`,`timestamp`) VALUES ('$content','$sysdatetime')";
				if ($conn_rrpl->query($sql) === FALSE) {
					throw new Exception("Error: ($sql)"); 
				} 

				$conn_rrpl->query("COMMIT");
				echo "
				<script>
				Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Consignor Updated.',
				showConfirmButton: false,
				timer: 1500
				})
				</script>";
			}
			catch(Exception $e) {
					$conn_rrpl->query("ROLLBACK"); 
					$content = $e->getMessage();
					$content = preg_replace("/[^0-9a-zA-Z ]/", "", $content);  
					echo "
					<script>
					Swal.fire({
					icon: 'error',
					title: 'Error !!!',
					text: '$content'
					})
					</script>";					
			}  
	 	} 
	 	else
	 	{
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: 'Consignor Can\'t be Same'
			})
			</script>";	  
	 	}

	} 

?>