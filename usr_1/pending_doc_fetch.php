<?php

  require('connect.php');
  
	$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
	$statement = $connection->prepare("select * from podmemo where sentby='COURIER' and docketno='' order by id desc");
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();

foreach($result as $row)
{ 
	$sub_array = array(); 
  
  $btn= "<center><button onclick='view(\"".$row['memono']."\")' class='btn btn-sm btn-primary' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-list'></i> VIEW  </button> </center>"; 
  $sub_array[] = $btn; 
	$sub_array[] = $row["memono"];
  $sub_array[] = $row["bill_party"]; 
  $sub_array[] = $row["branch"]; 
  $sub_array[] = $row["bill_branch"]; 
  $sub_array[] = $row["dispatchdate"]; 
  $sub_array[] = $row["sentby"]; 
  
  $sub_array[] = $row['couriername']; 
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 